package example.hdwallpaper.retrofit;

import example.hdwallpaper.model.AllWp.AllWpResponse;
import example.hdwallpaper.model.LatestWp.LatestWpResponse;
import example.hdwallpaper.model.WpCategory.CatWp.CatWpResponse;
import example.hdwallpaper.model.WpCategory.PopCat.PopCatResponse;
import example.hdwallpaper.model.WpCategory.AllCat.AllCatResponse;
import example.hdwallpaper.model.PopulerWp.PopWpResponse;
import example.hdwallpaper.model.ProWp.ProWpResponse;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface RetrofitService {


    @FormUrlEncoded
    @POST(Const.ADD_WALLPAPER)
    Call<AllWpResponse> fetchallwallpapaer(@Field(Const.FLAG) String flag,
                                           @Field(Const.OFFSET) String page);

    @FormUrlEncoded
    @POST(Const.ADD_WALLPAPER)
    Call<CatWpResponse> fetchcatwallpapaer(@Field(Const.FLAG) String flag,
                                           @Field(Const.WALLPAPER_CAT_NAME) String category);



    @FormUrlEncoded
    @POST(Const.ADD_CATEGORY)
    Call<AllCatResponse> fetchcategory(@Field(Const.FLAG) String flag);



    @FormUrlEncoded
    @POST(Const.POPULER)
    Call<PopCatResponse> fetchpopcategory(@Field(Const.FLAG) String flag);

    @FormUrlEncoded
    @POST(Const.PRO)
    Call<ProWpResponse> fetchProWp(@Field(Const.OFFSET) String page);

    @FormUrlEncoded
    @POST(Const.POPULER)
    Call<String> updatepopwallpapaer(@Field(Const.FLAG) String flag,
                                     @Field(Const.POP_WP) String pop_wp,
                                     @Field(Const.WP_ID) String wp_id);

    @FormUrlEncoded
    @POST(Const.POPULER)
    Call<PopWpResponse> fetchPopWallPapaer(@Field(Const.FLAG) String flag);



    @GET(Const.LATEST)
    Call<LatestWpResponse> fetchLatestWallPapaer();
}
