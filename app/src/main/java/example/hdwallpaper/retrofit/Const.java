package example.hdwallpaper.retrofit;

public interface Const {
    //String BASE_URL = "https://wecandoit.website/wallpaper/";
    String BASE_URL = "http://192.168.0.111/wallpaper/";
    String FLAG = "flag";

    String OFFSET = "page";
    String LIMIT = "limit";

    String ADD_WALLPAPER = "uploadwallpaper.php";
    String WP_IMAGE = "wp_imgname";
    String WP_ID="wp_id";
    String WP_IMGPATH="wp_imgpath";



    String ADD_CATEGORY = "uploadcategory.php";
    String WALLPAPER_CAT_ID = "cat_id";
    String WALLPAPER_CAT_NAME = "cat_name";
    String CAT_IMAGE = "cat_img";

    String POPULER="populer.php";
    String POP_WP="pop_wp";

    String PRO="prowp.php";

    String LATEST="latestwp.php";


    int UPLOAD_FLAG = 1;
    int FETCH_FLAG = 2;
    int DEL_FLAG = 3;

    int FETCHCAT_FLAG=4;
    int UPDET_FLAG=5;
    int FETCH_ALL_POP_CATEGORY_FLAG=7;

}
