package example.hdwallpaper.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.mlsdev.animatedrv.AnimatedRecyclerView;

import java.util.ArrayList;

import example.hdwallpaper.Glob;
import example.hdwallpaper.R;
import example.hdwallpaper.adapter.MyCatWpAdapter;
import example.hdwallpaper.model.WpCategory.CatWp.CatWpData;
import example.hdwallpaper.model.WpCategory.CatWp.CatWpResponse;
import example.hdwallpaper.retrofit.Const;
import example.hdwallpaper.retrofit.RetrofitClient;
import example.hdwallpaper.retrofit.RetrofitService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DisplayCatActivity extends AppCompatActivity {
    AnimatedRecyclerView listCatimg;
    Context context;
    ProgressDialog pd;
    RetrofitService service;
    private MyCatWpAdapter myCatWpAdapter;
    private ImageView imgBack;
    ArrayList<CatWpData> catWpDataArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_cat);

        context = this;
        service = RetrofitClient.getService();

        pd = new ProgressDialog(context);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        listCatimg = (AnimatedRecyclerView) findViewById(R.id.listCatimg);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 3);
        gridLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        listCatimg.setLayoutManager(gridLayoutManager);
        myCatWpAdapter = new MyCatWpAdapter(context, catWpDataArrayList, service);
        listCatimg.setAdapter(myCatWpAdapter);

        Toolbar catTool = (Toolbar) findViewById(R.id.catTool);
        catTool.setTitle(Glob.parCatName);
        fetchCategory();
    }

    private void fetchCategory() {
        Call<CatWpResponse> call = service.fetchcatwallpapaer(String.valueOf(Const.FETCHCAT_FLAG), Glob.parCatName);
        call.enqueue(new Callback<CatWpResponse>() {
            @Override
            public void onResponse(Call<CatWpResponse> call, Response<CatWpResponse> response) {
                catWpDataArrayList.addAll(response.body().getData());
                myCatWpAdapter.notifyDataSetChanged();
                listCatimg.scheduleLayoutAnimation();
                pd.dismiss();
            }

            @Override
            public void onFailure(Call<CatWpResponse> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(context, "No Data Found", Toast.LENGTH_SHORT).show();
                Log.d("er", t.getMessage());
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}
