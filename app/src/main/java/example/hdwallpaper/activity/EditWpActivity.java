package example.hdwallpaper.activity;

import android.os.Bundle;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import example.hdwallpaper.R;

public class EditWpActivity extends AppCompatActivity {

    private ImageView mWallpaper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_edit_wp);
        initView();


    }

    private void initView() {
        mWallpaper = (ImageView) findViewById(R.id.wallpaper);
       mWallpaper.setImageBitmap(TryalDisplayActivity.mainframebitmap);
    }


}
