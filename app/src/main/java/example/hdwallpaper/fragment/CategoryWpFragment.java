package example.hdwallpaper.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.mlsdev.animatedrv.AnimatedRecyclerView;

import java.util.ArrayList;

import example.hdwallpaper.BannerLayout.BannerLayout;
import example.hdwallpaper.Glob;
import example.hdwallpaper.R;
import example.hdwallpaper.adapter.MyFetchAllCatAdapter;
import example.hdwallpaper.adapter.MyFetchPopCatAdapter;
import example.hdwallpaper.model.WpCategory.AllCat.AllCatData;
import example.hdwallpaper.model.WpCategory.AllCat.AllCatResponse;
import example.hdwallpaper.model.WpCategory.PopCat.PopCatData;
import example.hdwallpaper.model.WpCategory.PopCat.PopCatResponse;
import example.hdwallpaper.retrofit.Const;
import example.hdwallpaper.retrofit.RetrofitClient;
import example.hdwallpaper.retrofit.RetrofitService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoryWpFragment extends Fragment {
    Context context;
    ProgressDialog pd;
    RetrofitService service;
    BannerLayout animatedRecyclerView;
    AnimatedRecyclerView CatList;

    ArrayList<PopCatData> popCatDataArrayList = new ArrayList<>();

    ArrayList<AllCatData> allCatDataArrayList = new ArrayList<>();

    private MyFetchPopCatAdapter myFetchPopCatAdapter;
    private MyFetchAllCatAdapter myFetchAllCatAdapter;

    public CategoryWpFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        context = getContext();
        View view = inflater.inflate(R.layout.fragment_category_wp, container, false);
        service = RetrofitClient.getService();
        pd = new ProgressDialog(context);
        animatedRecyclerView = view.findViewById(R.id.popCatList);
        CatList = view.findViewById(R.id.CatList);


        GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 2);
        gridLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        CatList.setLayoutManager(gridLayoutManager);


        pd.setTitle("Please Wait");
        pd.setMessage("Fetch Category");
        pd.show();
        fetchPopulerCategory();
        fetchCategory();
        return view;
    }

    private void fetchPopulerCategory() {
        Call<PopCatResponse> call = service.fetchpopcategory(String.valueOf(Const.FETCH_ALL_POP_CATEGORY_FLAG));
        call.enqueue(new Callback<PopCatResponse>() {
            @Override
            public void onResponse(Call<PopCatResponse> call, Response<PopCatResponse> response) {
                popCatDataArrayList.addAll(response.body().getData());
                myFetchPopCatAdapter = new MyFetchPopCatAdapter(context, popCatDataArrayList);
                animatedRecyclerView.setAdapter(myFetchPopCatAdapter);
                myFetchPopCatAdapter.notifyDataSetChanged();
                animatedRecyclerView.scheduleLayoutAnimation();
                pd.dismiss();
            }

            @Override
            public void onFailure(Call<PopCatResponse> call, Throwable t) {
                Log.d("apiResponse",t.getMessage());
            }
        });
    }

    private void fetchCategory() {
        Call<AllCatResponse> call = service.fetchcategory(String.valueOf(Const.FETCH_FLAG));
        call.enqueue(new Callback<AllCatResponse>() {
            @Override
            public void onResponse(Call<AllCatResponse> call, Response<AllCatResponse> response) {
                allCatDataArrayList.addAll(response.body().getData());
                myFetchAllCatAdapter = new MyFetchAllCatAdapter(context,allCatDataArrayList);
                CatList.setAdapter(myFetchAllCatAdapter);
                myFetchAllCatAdapter.notifyDataSetChanged();
                CatList.scheduleLayoutAnimation();
                pd.dismiss();
            }

            @Override
            public void onFailure(Call<AllCatResponse> call, Throwable t) {

            }
        });
    }
}
