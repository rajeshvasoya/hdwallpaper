package example.hdwallpaper.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import example.hdwallpaper.R;
import example.hdwallpaper.adapter.MyProWpAdapter;
import example.hdwallpaper.model.ProWp.ProWpData;
import example.hdwallpaper.model.ProWp.ProWpResponse;
import example.hdwallpaper.retrofit.RetrofitClient;
import example.hdwallpaper.retrofit.RetrofitService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
@RequiresApi(api = Build.VERSION_CODES.M)
public class ProWpFragment extends Fragment {
    Context context;
    ProgressDialog pd;
    RetrofitService service;
    RecyclerView proWallRecycler;
    MyProWpAdapter myProWpAdapter;

    private int page = 1;
    private boolean loading = true;

    private GridLayoutManager gridLayoutManager;

    ArrayList<ProWpData> proWpDataArrayList = new ArrayList<>();

    public ProWpFragment() {
        // Required empty public constructor
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        context = getContext();

        View view = inflater.inflate(R.layout.fragment_pro_wp, container, false);
        service = RetrofitClient.getService();
        proWpDataArrayList.clear();
        pd = new ProgressDialog(context);
        proWallRecycler = (RecyclerView) view.findViewById(R.id.proWallRecycler);
        gridLayoutManager = new GridLayoutManager(context, 3);
        gridLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        proWallRecycler.setLayoutManager(gridLayoutManager);
        myProWpAdapter = new MyProWpAdapter(context, proWpDataArrayList, service);
        proWallRecycler.setAdapter(myProWpAdapter);

        pd.setTitle("Please Wait");
        pd.setMessage("Fetch Category");
        pd.show();
        fetchProWallpaper(page);

        proWallRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) //check for scroll down
                {
                   int visibleItemCount = gridLayoutManager.getChildCount();
                   int totalItemCount = gridLayoutManager.getItemCount();
                   int pastVisiblesItems = gridLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = true;
                            page = page + 1;
                            fetchProWallpaper(page);
                            //Toast.makeText(context, "Last Item Wow !", Toast.LENGTH_SHORT).show();
                            Log.v("...", "Last Item Wow !");
                            //Do pagination.. i.e. fetch new data
                        }
                    }
                }
            }
        });
        return view;
    }


    private void fetchProWallpaper(int i) {

        Call<ProWpResponse> call = service.fetchProWp(String.valueOf(i));
        call.enqueue(new Callback<ProWpResponse>() {
            @Override
            public void onResponse(Call<ProWpResponse> call, Response<ProWpResponse> response) {
                proWpDataArrayList.addAll(response.body().getData());
                myProWpAdapter.notifyDataSetChanged();

                pd.dismiss();
            }

            @Override
            public void onFailure(Call<ProWpResponse> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(context, "No Data Found", Toast.LENGTH_SHORT).show();
                Log.d("er", t.toString());
            }
        });
    }


}
