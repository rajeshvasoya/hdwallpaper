package example.hdwallpaper.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import example.hdwallpaper.Glob;
import example.hdwallpaper.R;
import example.hdwallpaper.adapter.MyFavWpAdapter;
import example.hdwallpaper.adapter.MyLatestWpAdapter;
import example.hdwallpaper.model.MyFav.FavoriteList;
import example.hdwallpaper.retrofit.RetrofitClient;
import example.hdwallpaper.retrofit.RetrofitService;

/**
 * A simple {@link Fragment} subclass.
 */
public class FavouritesWpFragment extends Fragment {

    ProgressDialog pd;
    RetrofitService service;
    MyFavWpAdapter myFavWpAdapter;
    ArrayList<FavoriteList> favoriteLists = new ArrayList<>();
    private RecyclerView mFavouriteRecycle;


    public FavouritesWpFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_favourites_wp, container, false);
    }

    private void fetchFavWallpaper() {
        favoriteLists.clear();
        favoriteLists.addAll(Glob.favoriteDatabase.favoriteDao().getFavoriteData());
        myFavWpAdapter.notifyDataSetChanged();
    }


    @Override
    public void onStart() {
        super.onStart();
        findView();
    }

    private void findView() {
        View view=getView();
        service = RetrofitClient.getService();

        mFavouriteRecycle = (RecyclerView) view.findViewById(R.id.favourite);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 3);
        mFavouriteRecycle.setLayoutManager(gridLayoutManager);
        myFavWpAdapter = new MyFavWpAdapter(getActivity(), favoriteLists);
        mFavouriteRecycle.setAdapter(myFavWpAdapter);

        fetchFavWallpaper();
    }
}
