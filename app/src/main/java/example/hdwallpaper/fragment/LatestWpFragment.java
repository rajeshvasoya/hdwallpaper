package example.hdwallpaper.fragment;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import example.hdwallpaper.R;
import example.hdwallpaper.adapter.MyLatestWpAdapter;
import example.hdwallpaper.model.LatestWp.LatestWpData;
import example.hdwallpaper.model.LatestWp.LatestWpResponse;
import example.hdwallpaper.retrofit.RetrofitClient;
import example.hdwallpaper.retrofit.RetrofitService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class LatestWpFragment extends Fragment {
    private RecyclerView mLatestWallRecycler;
    ProgressDialog pd;
    RetrofitService service;
    MyLatestWpAdapter myLatestWpAdapter;
    ArrayList<LatestWpData> latestWpDataArrayList = new ArrayList<>();

    public LatestWpFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_latest_wp, container, false);
        service = RetrofitClient.getService();
        latestWpDataArrayList.clear();
        pd = new ProgressDialog(getContext());

        mLatestWallRecycler = (RecyclerView) view.findViewById(R.id.LatestWallRecycler);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 3);
        gridLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mLatestWallRecycler.setLayoutManager(gridLayoutManager);
        myLatestWpAdapter = new MyLatestWpAdapter(getActivity(), latestWpDataArrayList, service);
        mLatestWallRecycler.setAdapter(myLatestWpAdapter);

        pd.setTitle("Please Wait");
        pd.setMessage("Fetch Category");
        pd.show();
        fetchLatestWallpaper();
        return view;
    }

    private void fetchLatestWallpaper() {

        Call<LatestWpResponse> call = service.fetchLatestWallPapaer();
        call.enqueue(new Callback<LatestWpResponse>() {
            @Override
            public void onResponse(Call<LatestWpResponse> call, Response<LatestWpResponse> response) {
                latestWpDataArrayList.addAll(response.body().getData());
                myLatestWpAdapter.notifyDataSetChanged();
                pd.dismiss();
            }

            @Override
            public void onFailure(Call<LatestWpResponse> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(getContext(), "No Data Found", Toast.LENGTH_SHORT).show();
                Log.d("er", t.toString());
            }
        });
    }

}
