package example.hdwallpaper.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import example.hdwallpaper.Glob;
import example.hdwallpaper.R;
import example.hdwallpaper.activity.TryalDisplayActivity;
import example.hdwallpaper.model.LatestWp.LatestWpData;
import example.hdwallpaper.model.WPData.WpData;
import example.hdwallpaper.retrofit.Const;
import example.hdwallpaper.retrofit.RetrofitService;

public class MyLatestWpAdapter extends RecyclerView.Adapter<MyLatestWpAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<LatestWpData> latestWpDataArrayList;
    private RetrofitService service;

    ArrayList<WpData> wpData=new ArrayList<>();



    public MyLatestWpAdapter(Context context, ArrayList<LatestWpData> latestWpDataArrayList, RetrofitService service) {
        this.latestWpDataArrayList = latestWpDataArrayList;
        this.context = context;
        this.service = service;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.wallpaper_raw_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        final LatestWpData data=latestWpDataArrayList.get(position);
        Glide.with(context)
                .load(Const.BASE_URL + data.getWpImgpath())
                .into(holder.my_imageview);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wpData.clear();
                for (int i=0;i<latestWpDataArrayList.size();i++){
                    WpData wpDatas=new WpData();
                    wpDatas.setWpId(latestWpDataArrayList.get(i).getWpId());
                    wpDatas.setCatId(latestWpDataArrayList.get(i).getCatId());
                    wpDatas.setCatName(latestWpDataArrayList.get(i).getCatName());
                    wpDatas.setWpImgname(latestWpDataArrayList.get(i).getWpImgname());
                    wpDatas.setWpImgpath(latestWpDataArrayList.get(i).getWpImgpath());
                    wpDatas.setWpTime(latestWpDataArrayList.get(i).getWpTime());
                    wpDatas.setPopWp(latestWpDataArrayList.get(i).getPopWp());
                    wpData.add(wpDatas);
                }
                Glob.wpPosition=position;
                Intent intent=new Intent(context,TryalDisplayActivity.class);
                intent.putExtra("wpData",wpData);
                context.startActivity(intent);
                //Toast.makeText(context, ""+wpData.size(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return latestWpDataArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView my_imageview;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            my_imageview = (ImageView) itemView.findViewById(R.id.my_imageview);
        }
    }
}
