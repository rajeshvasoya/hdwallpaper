package example.hdwallpaper.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import example.hdwallpaper.R;
import example.hdwallpaper.activity.TryalDisplayActivity;
import example.hdwallpaper.model.WPData.WpData;
import example.hdwallpaper.retrofit.Const;


public class MyTryalPagerAdapter extends PagerAdapter {


    private TryalDisplayActivity mContext;
    private ViewPager mViewPager;

    private ArrayList<WpData> wpData;



    public MyTryalPagerAdapter(ArrayList<WpData> wpData, TryalDisplayActivity context, ViewPager mViewPager) {
        this.wpData = wpData;
        this.mContext = context;
        this.mViewPager = mViewPager;

    }


    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View inflate = LayoutInflater.from(container.getContext()).inflate(R.layout.cardviewpager_item, container, false);
        ImageView imageView1 = (ImageView) inflate.findViewById(R.id.img_card_item);

        Glide.with(mContext).load(Const.BASE_URL + wpData.get(position).getWpImgpath()).into(imageView1);

        container.addView(inflate);
        return inflate;
    }

    @Override
    public int getCount() {
        return wpData.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView(((View) object));
    }

}
