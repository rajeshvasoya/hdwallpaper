package example.hdwallpaper.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import example.hdwallpaper.Glob;
import example.hdwallpaper.R;
import example.hdwallpaper.activity.DisplayCatActivity;
import example.hdwallpaper.model.WpCategory.AllCat.AllCatData;
import example.hdwallpaper.model.WpCategory.AllCat.AllCatResponse;
import example.hdwallpaper.retrofit.Const;

public class MyFetchAllCatAdapter extends RecyclerView.Adapter<MyFetchAllCatAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<AllCatData> allCatDataArrayList;


    public MyFetchAllCatAdapter(Context context, ArrayList<AllCatData> allCatDataArrayList) {
        this.context = context;
        this.allCatDataArrayList = allCatDataArrayList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.category_item_list, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        final AllCatData data=allCatDataArrayList.get(position);
        Glide.with(context)
                .load(Const.BASE_URL+data.getCatImgpath())
                .into(holder.my_imageview);
        holder.txtCatName.setText(data.getCatName());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Glob.parCatName=data.getCatName();
                context.startActivity(new Intent(context, DisplayCatActivity.class));

            }
        });
    }

    @Override
    public int getItemCount() {
        return allCatDataArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView my_imageview;
        TextView txtCatName;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            my_imageview =(ImageView) itemView.findViewById(R.id.catImgView);
            txtCatName =(TextView) itemView.findViewById(R.id.txtCatName);
        }
    }
}
