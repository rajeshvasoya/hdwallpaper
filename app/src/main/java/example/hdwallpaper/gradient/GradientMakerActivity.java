package example.hdwallpaper.gradient;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.WallpaperManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.balysv.materialripple.MaterialRippleLayout;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.tapadoo.alerter.Alerter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import example.hdwallpaper.Glob;
import example.hdwallpaper.R;
import example.hdwallpaper.model.RgbCol.RGB;
import example.hdwallpaper.utils.other.Constants;

import static example.hdwallpaper.Glob.myDir;

@SuppressLint("WrongConstant")
public class GradientMakerActivity extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {
    /* access modifiers changed from: private */
    public SeekBar b;
    /* access modifiers changed from: private */
    public TextView bValue;
    /* access modifiers changed from: private */
    public RelativeLayout background;
    private RadioButton bestFit;
    /* access modifiers changed from: private */
    public Bitmap bitmap;
    /* access modifiers changed from: private */
    public ProgressBar blurPb;
    /* access modifiers changed from: private */
    public TextView blurPercentage;
    /* access modifiers changed from: private */
    public int blurProgress;
    /* access modifiers changed from: private */
    public ImageButton colorFive;
    /* access modifiers changed from: private */
    public RGB colorFiveRGB;
    /* access modifiers changed from: private */
    public ImageButton colorFour;
    /* access modifiers changed from: private */
    public RGB colorFourRGB;
    /* access modifiers changed from: private */
    public ImageButton colorOne;
    /* access modifiers changed from: private */
    public RGB colorOneRGB;
    /* access modifiers changed from: private */
    public ImageButton colorThree;
    /* access modifiers changed from: private */
    public RGB colorThreeRGB;
    /* access modifiers changed from: private */
    public ImageButton colorTwo;
    /* access modifiers changed from: private */
    public RGB colorTwoRGB;
    /* access modifiers changed from: private */
    public int[] colors = new int[5];
    /* access modifiers changed from: private */
    public SeekBar g;
    /* access modifiers changed from: private */
    public TextView gValue;
    private GradientDrawable gradient;
    private RelativeLayout gradientOptions;
    private int gradientType = 0;
    /* access modifiers changed from: private */
    public File gradients;
    /* access modifiers changed from: private */
    public ImageView imageView;
    private boolean isVisible;
    private SeekBar.OnSeekBarChangeListener onSeekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        public void onStopTrackingTouch(SeekBar seekBar) {
        }

        public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
            if (seekBar.getId() != R.id.radius) {
                GradientMakerActivity.this.background.setBackgroundColor(Color.rgb(GradientMakerActivity.this.r.getProgress(), GradientMakerActivity.this.g.getProgress(), GradientMakerActivity.this.b.getProgress()));
            }
            if (seekBar.getId() == R.id.r) {
                GradientMakerActivity.this.rValue.setText(String.valueOf(i));
            } else if (seekBar.getId() == R.id.g) {
                GradientMakerActivity.this.gValue.setText(String.valueOf(i));
            } else if (seekBar.getId() == R.id.b) {
                GradientMakerActivity.this.bValue.setText(String.valueOf(i));
            } else {
                GradientMakerActivity.this.invalidateGradient();
            }
        }
    };
    private GradientDrawable.Orientation orientation = GradientDrawable.Orientation.TOP_BOTTOM;
    private LinearLayout orientations;
    private Point point;
    /* access modifiers changed from: private */
    public SeekBar r;
    /* access modifiers changed from: private */
    public TextView rValue;
    private LinearLayout radialOptions;
    private SeekBar radius;
    /* access modifiers changed from: private */
    public SharedPreferences sharedPreferences;
    private RelativeLayout toolbar;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_gradient_maker);
        this.sharedPreferences = getSharedPreferences(Constants.SHARED_PREFERENCES_NAME, 0);
        if (!this.sharedPreferences.getBoolean("TourGuide", false)) {
            showTourGuide();
        }
        findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                GradientMakerActivity.this.finish();
            }
        });
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().setFlags(512, 512);
        }
        Display defaultDisplay = ((WindowManager) getSystemService(WINDOW_SERVICE)).getDefaultDisplay();
        this.point = new Point();
        if (Build.VERSION.SDK_INT >= 19) {
            defaultDisplay.getRealSize(this.point);
        } else {
            defaultDisplay.getSize(this.point);
        }
        this.radius = (SeekBar) findViewById(R.id.radius);
        this.radius.setMax(this.point.y * 2);
        SeekBar seekBar = this.radius;
        seekBar.setProgress(seekBar.getMax() / 2);
        this.radius.setOnSeekBarChangeListener(this.onSeekBarChangeListener);
        this.gradient = new GradientDrawable();
        this.colorOne = (ImageButton) findViewById(R.id.color_one);
        this.colorTwo = (ImageButton) findViewById(R.id.color_two);
        this.colorThree = (ImageButton) findViewById(R.id.color_three);
        this.colorFour = (ImageButton) findViewById(R.id.color_four);
        this.colorFive = (ImageButton) findViewById(R.id.color_five);
        this.imageView = (ImageView) findViewById(R.id.imageView);
        this.gradientOptions = (RelativeLayout) findViewById(R.id.gradient_options);
        this.radialOptions = (LinearLayout) findViewById(R.id.radial_option);
        this.orientations = (LinearLayout) findViewById(R.id.orientations);
        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.radio_group);
        radioGroup.setOnCheckedChangeListener(this);
        radioGroup.check(R.id.type_linear);
        int randomNumber = getRandomNumber();
        int randomNumber2 = getRandomNumber();
        int randomNumber3 = getRandomNumber();
        int randomNumber4 = getRandomNumber();
        int randomNumber5 = getRandomNumber();
        int randomNumber6 = getRandomNumber();
        int rgb = Color.rgb(randomNumber, randomNumber2, randomNumber3);
        int rgb2 = Color.rgb(randomNumber4, randomNumber5, randomNumber6);
        int[] iArr = this.colors;
        iArr[0] = rgb;
        iArr[1] = rgb2;
        iArr[2] = -1;
        iArr[3] = -1;
        iArr[4] = -1;
        this.colorOne.setColorFilter(rgb);
        this.colorTwo.setColorFilter(rgb2);
        this.colorOneRGB = new RGB(randomNumber, randomNumber2, randomNumber3);
        this.colorTwoRGB = new RGB(randomNumber4, randomNumber5, randomNumber6);
        this.colorThreeRGB = new RGB(getRandomNumber(), getRandomNumber(), getRandomNumber());
        this.colorFourRGB = new RGB(getRandomNumber(), getRandomNumber(), getRandomNumber());
        this.colorFiveRGB = new RGB(getRandomNumber(), getRandomNumber(), getRandomNumber());
        this.toolbar = (RelativeLayout) findViewById(R.id.toolbar);
        this.colorOne.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                GradientMakerActivity.this.showColorPickerDialog(view);
            }
        });
        this.colorTwo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                GradientMakerActivity.this.showColorPickerDialog(view);
            }
        });
        this.colorThree.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                GradientMakerActivity.this.showColorPickerDialog(view);
            }
        });
        this.colorFour.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (GradientMakerActivity.this.getActualLengthOfColorsArray() >= 3) {
                    GradientMakerActivity.this.showColorPickerDialog(view);
                } else {
                    Toast.makeText(GradientMakerActivity.this, "You need to select the previous color first", Toast.LENGTH_SHORT).show();
                }
            }
        });
        this.colorFive.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                if (GradientMakerActivity.this.getActualLengthOfColorsArray() >= 4) {
                    GradientMakerActivity.this.showColorPickerDialog(view);
                } else {
                    Toast.makeText(GradientMakerActivity.this, "You need to select the previous color first", 0).show();
                }
            }
        });
        ImageButton imageButton = (ImageButton) findViewById(R.id.apply_random_colors);
        if (Build.VERSION.SDK_INT >= 21) {
            imageButton.setBackgroundResource(R.drawable.circular_button);
        }
        imageButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                GradientMakerActivity.this.applyRandomColors();
            }
        });
        ImageButton imageButton2 = (ImageButton) findViewById(R.id.top_left);
        ImageButton imageButton3 = (ImageButton) findViewById(R.id.down_left);
        ImageButton imageButton4 = (ImageButton) findViewById(R.id.right);
        ImageButton imageButton5 = (ImageButton) findViewById(R.id.top_right);
        ImageButton imageButton6 = (ImageButton) findViewById(R.id.down_right);
        ImageButton imageButton7 = (ImageButton) findViewById(R.id.top);
        ImageButton imageButton8 = (ImageButton) findViewById(R.id.down);
        ((ImageButton) findViewById(R.id.left)).setOnClickListener(this);
        imageButton2.setOnClickListener(this);
        imageButton3.setOnClickListener(this);
        imageButton4.setOnClickListener(this);
        imageButton5.setOnClickListener(this);
        imageButton6.setOnClickListener(this);
        imageButton7.setOnClickListener(this);
        imageButton8.setOnClickListener(this);
        this.imageView.setOnClickListener(this);
        ImageButton imageButton9 = (ImageButton) findViewById(R.id.apply);
        ((ImageButton) findViewById(R.id.save)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                GradientMakerActivity.this.saveGradient();
            }
        });
        imageButton9.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                GradientMakerActivity.this.applyWallpaper();
            }
        });
        invalidateGradient();
        createDirectories();
    }

    private void showTourGuide() {
        new TapTargetSequence((Activity) this).targets(TapTarget.forView(findViewById(R.id.type_linear), "Gradient Types", "Choose the different gradient types.").cancelable(false).outerCircleColor(R.color.light).targetCircleColor(R.color.soDark).titleTextColor(R.color.black).descriptionTextColor(R.color.soDark), TapTarget.forView(findViewById(R.id.color_one), "Gradient Colors", "You can add up to 5 colors for each type.").cancelable(false).outerCircleColor(R.color.light).targetCircleColor(R.color.soDark).titleTextColor(R.color.black).descriptionTextColor(R.color.soDark).tintTarget(false), TapTarget.forView(findViewById(R.id.top), "Gradient Angles", "Only for Linear Gradients. Defines the color shifting angle.").cancelable(false).outerCircleColor(R.color.light).targetCircleColor(R.color.soDark).titleTextColor(R.color.black).descriptionTextColor(R.color.soDark), TapTarget.forView(findViewById(R.id.apply_random_colors), "Random Colors", "Tap for getting random colors for gradient.").cancelable(false).outerCircleColor(R.color.light).targetCircleColor(R.color.soDark).titleTextColor(R.color.black).descriptionTextColor(R.color.soDark).tintTarget(false), TapTarget.forView(findViewById(R.id.apply), "Apply as Wallpaper", "Tap to apply the gradient as wallpaper.").cancelable(false).outerCircleColor(R.color.light).targetCircleColor(R.color.soDark).titleTextColor(R.color.black).descriptionTextColor(R.color.soDark), TapTarget.forView(findViewById(R.id.save), "Save", "Click to download the created gradient.").cancelable(false).outerCircleColor(R.color.light).targetCircleColor(R.color.soDark).titleTextColor(R.color.black).descriptionTextColor(R.color.soDark)).listener(new TapTargetSequence.Listener() {
            public void onSequenceCanceled(TapTarget tapTarget) {
            }

            public void onSequenceStep(TapTarget tapTarget, boolean z) {
            }

            public void onSequenceFinish() {
                GradientMakerActivity.this.sharedPreferences.edit().putBoolean("TourGuide", true).apply();
            }
        }).start();
    }

    /* access modifiers changed from: private */
    public void applyRandomColors() {
        int actualLengthOfColorsArray = getActualLengthOfColorsArray();
        if (actualLengthOfColorsArray == 5) {
            this.colorOneRGB.setRed(getRandomNumber());
            this.colorOneRGB.setBlue(getRandomNumber());
            this.colorOneRGB.setGreen(getRandomNumber());
            this.colorTwoRGB.setRed(getRandomNumber());
            this.colorTwoRGB.setBlue(getRandomNumber());
            this.colorTwoRGB.setGreen(getRandomNumber());
            this.colorThreeRGB.setRed(getRandomNumber());
            this.colorThreeRGB.setBlue(getRandomNumber());
            this.colorThreeRGB.setGreen(getRandomNumber());
            this.colorFourRGB.setRed(getRandomNumber());
            this.colorFourRGB.setBlue(getRandomNumber());
            this.colorFourRGB.setGreen(getRandomNumber());
            this.colorFiveRGB.setRed(getRandomNumber());
            this.colorFiveRGB.setBlue(getRandomNumber());
            this.colorFiveRGB.setGreen(getRandomNumber());
            this.colors[0] = Color.rgb(this.colorOneRGB.getRed(), this.colorOneRGB.getGreen(), this.colorOneRGB.getBlue());
            this.colors[1] = Color.rgb(this.colorTwoRGB.getRed(), this.colorTwoRGB.getGreen(), this.colorTwoRGB.getBlue());
            this.colors[2] = Color.rgb(this.colorThreeRGB.getRed(), this.colorThreeRGB.getGreen(), this.colorThreeRGB.getBlue());
            this.colors[3] = Color.rgb(this.colorFourRGB.getRed(), this.colorFourRGB.getGreen(), this.colorFourRGB.getBlue());
            this.colors[4] = Color.rgb(this.colorFiveRGB.getRed(), this.colorFiveRGB.getGreen(), this.colorFiveRGB.getBlue());
            this.colorOne.setColorFilter(this.colors[0]);
            this.colorTwo.setColorFilter(this.colors[1]);
            this.colorThree.setColorFilter(this.colors[2]);
            this.colorFour.setColorFilter(this.colors[3]);
            this.colorFive.setColorFilter(this.colors[4]);
        } else if (actualLengthOfColorsArray == 4) {
            this.colorOneRGB.setRed(getRandomNumber());
            this.colorOneRGB.setBlue(getRandomNumber());
            this.colorOneRGB.setGreen(getRandomNumber());
            this.colorTwoRGB.setRed(getRandomNumber());
            this.colorTwoRGB.setBlue(getRandomNumber());
            this.colorTwoRGB.setGreen(getRandomNumber());
            this.colorThreeRGB.setRed(getRandomNumber());
            this.colorThreeRGB.setBlue(getRandomNumber());
            this.colorThreeRGB.setGreen(getRandomNumber());
            this.colorFourRGB.setRed(getRandomNumber());
            this.colorFourRGB.setBlue(getRandomNumber());
            this.colorFourRGB.setGreen(getRandomNumber());
            this.colors[0] = Color.rgb(this.colorOneRGB.getRed(), this.colorOneRGB.getGreen(), this.colorOneRGB.getBlue());
            this.colors[1] = Color.rgb(this.colorTwoRGB.getRed(), this.colorTwoRGB.getGreen(), this.colorTwoRGB.getBlue());
            this.colors[2] = Color.rgb(this.colorThreeRGB.getRed(), this.colorThreeRGB.getGreen(), this.colorThreeRGB.getBlue());
            this.colors[3] = Color.rgb(this.colorFourRGB.getRed(), this.colorFourRGB.getGreen(), this.colorFourRGB.getBlue());
            this.colorOne.setColorFilter(this.colors[0]);
            this.colorTwo.setColorFilter(this.colors[1]);
            this.colorThree.setColorFilter(this.colors[2]);
            this.colorFour.setColorFilter(this.colors[3]);
        } else if (actualLengthOfColorsArray == 3) {
            this.colorOneRGB.setRed(getRandomNumber());
            this.colorOneRGB.setBlue(getRandomNumber());
            this.colorOneRGB.setGreen(getRandomNumber());
            this.colorTwoRGB.setRed(getRandomNumber());
            this.colorTwoRGB.setBlue(getRandomNumber());
            this.colorTwoRGB.setGreen(getRandomNumber());
            this.colorThreeRGB.setRed(getRandomNumber());
            this.colorThreeRGB.setBlue(getRandomNumber());
            this.colorThreeRGB.setGreen(getRandomNumber());
            this.colors[0] = Color.rgb(this.colorOneRGB.getRed(), this.colorOneRGB.getGreen(), this.colorOneRGB.getBlue());
            this.colors[1] = Color.rgb(this.colorTwoRGB.getRed(), this.colorTwoRGB.getGreen(), this.colorTwoRGB.getBlue());
            this.colors[2] = Color.rgb(this.colorThreeRGB.getRed(), this.colorThreeRGB.getGreen(), this.colorThreeRGB.getBlue());
            this.colorOne.setColorFilter(this.colors[0]);
            this.colorTwo.setColorFilter(this.colors[1]);
            this.colorThree.setColorFilter(this.colors[2]);
        } else if (actualLengthOfColorsArray == 2) {
            this.colorOneRGB.setRed(getRandomNumber());
            this.colorOneRGB.setBlue(getRandomNumber());
            this.colorOneRGB.setGreen(getRandomNumber());
            this.colorTwoRGB.setRed(getRandomNumber());
            this.colorTwoRGB.setBlue(getRandomNumber());
            this.colorTwoRGB.setGreen(getRandomNumber());
            this.colors[0] = Color.rgb(this.colorOneRGB.getRed(), this.colorOneRGB.getGreen(), this.colorOneRGB.getBlue());
            this.colors[1] = Color.rgb(this.colorTwoRGB.getRed(), this.colorTwoRGB.getGreen(), this.colorTwoRGB.getBlue());
            this.colorOne.setColorFilter(this.colors[0]);
            this.colorTwo.setColorFilter(this.colors[1]);
        } else if (actualLengthOfColorsArray == 1) {
            this.colorOneRGB.setRed(getRandomNumber());
            this.colorOneRGB.setBlue(getRandomNumber());
            this.colorOneRGB.setGreen(getRandomNumber());
            this.colors[0] = Color.rgb(this.colorOneRGB.getRed(), this.colorOneRGB.getGreen(), this.colorOneRGB.getBlue());
            this.colorOne.setColorFilter(this.colors[0]);
        }
        invalidateGradient();
    }

    /* access modifiers changed from: private */
    public void applyWallpaper() {
        String str = "%";
        String str2 = "Set this wallpaper on?";
        if (Build.VERSION.SDK_INT >= 24) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle((CharSequence) str2);
            View inflate = getLayoutInflater().inflate(R.layout.onlysetwallpaper, null);
            MaterialRippleLayout materialRippleLayout = (MaterialRippleLayout) inflate.findViewById(R.id.Home);
            MaterialRippleLayout materialRippleLayout2 = (MaterialRippleLayout) inflate.findViewById(R.id.Lock);
            MaterialRippleLayout materialRippleLayout3 = (MaterialRippleLayout) inflate.findViewById(R.id.Both);
            builder.setView(inflate);
            final AlertDialog create = builder.create();
            create.setCancelable(true);
            materialRippleLayout.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    create.dismiss();
                    GradientMakerActivity.this.downloadAndSet("homescreen");
                }
            });
            materialRippleLayout2.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    create.dismiss();
                    GradientMakerActivity.this.downloadAndSet("lockscreen");
                }
            });
            materialRippleLayout3.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    create.dismiss();
                    GradientMakerActivity.this.downloadAndSet("bothscreen");
                }
            });
            create.getWindow().setFlags(512, 512);
            create.setOnDismissListener(new DialogInterface.OnDismissListener() {
                public void onDismiss(DialogInterface dialogInterface) {
                    GradientMakerActivity.this.hideNavigationBar();
                }
            });
            create.show();
            return;
        }
        AlertDialog.Builder builder2 = new AlertDialog.Builder(this);
        builder2.setTitle((CharSequence) str2);
        View inflate2 = getLayoutInflater().inflate(R.layout.setwallpaper_just_home, null);
        MaterialRippleLayout materialRippleLayout4 = (MaterialRippleLayout) inflate2.findViewById(R.id.Home);
        builder2.setView(inflate2);
        final AlertDialog create2 = builder2.create();
        create2.setCancelable(true);
        materialRippleLayout4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                create2.dismiss();
                GradientMakerActivity.this.downloadAndSet("homescreen");
            }
        });
        create2.getWindow().setFlags(512, 512);
        create2.setOnDismissListener(new DialogInterface.OnDismissListener() {
            public void onDismiss(DialogInterface dialogInterface) {
                GradientMakerActivity.this.hideNavigationBar();
            }
        });
        create2.show();
    }

    public void downloadAndSet(String str) {
        String str2 = "Please wait...";
        if (str.equalsIgnoreCase("homescreen")) {
            Alerter.create(this).setTitle(str2).setText("Setting wallpaper on your homescreen").setBackgroundColor(R.color.green).enableInfiniteDuration(true).show();
            setAsWallpaper("home");
        } else if (str.equalsIgnoreCase("lockscreen")) {
            Alerter.create(this).setTitle(str2).setText("Setting wallpaper on your lockscreen").setBackgroundColor(R.color.green).enableInfiniteDuration(true).show();
            setAsWallpaper("lock");
        } else {
            Alerter.create(this).setTitle(str2).setText("Setting wallpaper on both screens").setBackgroundColor(R.color.green).enableInfiniteDuration(true).show();
            setAsWallpaper("both");
        }
    }

    public void setAsWallpaper(final String str) {
        final Bitmap bitmap2;
        final WallpaperManager instance = WallpaperManager.getInstance(this);

            bitmap2 = this.bitmap;

        new Thread(new Runnable() {
            @SuppressLint("WrongConstant")
            public void run() {
                try {
                    char c = 65535;
                    int hashCode = str.hashCode();
                    if (hashCode != 3208415) {
                        if (hashCode == 3327275) {
                            if (str.equals("lock")) {
                                c = 1;
                            }
                        }
                    } else if (str.equals("home")) {
                        c = 0;
                    }
                    if (c != 0) {
                        if (c != 1) {
                            if (Build.VERSION.SDK_INT >= 24) {
                                instance.setBitmap(bitmap2, null, true, 1);
                                instance.setBitmap(bitmap2, null, true, 2);
                            }
                        } else if (Build.VERSION.SDK_INT >= 24) {
                            instance.setBitmap(bitmap2, null, true, 2);
                        }
                    } else if (Build.VERSION.SDK_INT >= 24) {
                        instance.setBitmap(bitmap2, null, true, 1);
                    } else {
                        instance.setBitmap(bitmap2);
                    }
                    Alerter.create(GradientMakerActivity.this).setTitle("Wallpaper Updated!").setBackgroundColor(R.color.green).setDuration(1000).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private void createDirectories() {
        File file = new File(Environment.getExternalStorageDirectory(), new Constants(this).getFolderName());
        if (!file.exists()) {
            file.mkdir();
        }
        this.gradients = new File(file, "/Gradients");
        if (!this.gradients.exists()) {
            this.gradients.mkdir();
        }
    }

    public void onClick(View view) {
        if (view.getId() == R.id.left) {
            this.orientation = GradientDrawable.Orientation.LEFT_RIGHT;
        } else if (view.getId() == R.id.top_left) {
            this.orientation = GradientDrawable.Orientation.TL_BR;
        } else if (view.getId() == R.id.down_left) {
            this.orientation = GradientDrawable.Orientation.BL_TR;
        } else if (view.getId() == R.id.right) {
            this.orientation = GradientDrawable.Orientation.RIGHT_LEFT;
        } else if (view.getId() == R.id.top_right) {
            this.orientation = GradientDrawable.Orientation.TR_BL;
        } else if (view.getId() == R.id.down_right) {
            this.orientation = GradientDrawable.Orientation.BR_TL;
        } else if (view.getId() == R.id.top) {
            this.orientation = GradientDrawable.Orientation.TOP_BOTTOM;
        } else if (view.getId() == R.id.down) {
            this.orientation = GradientDrawable.Orientation.BOTTOM_TOP;
        } else if (view.getId() == R.id.imageView) {
            changeVisibility();
            return;
        }
        invalidateGradient();
    }

    private void changeVisibility() {
        if (this.isVisible) {
            this.isVisible = false;
            this.toolbar.setVisibility(8);
            this.gradientOptions.setVisibility(8);
            return;
        }
        this.isVisible = true;
        this.toolbar.setVisibility(0);
        this.gradientOptions.setVisibility(0);
    }

    /* access modifiers changed from: private */
    public void invalidateGradient() {
        int[] iArr = new int[getActualLengthOfColorsArray()];
        System.arraycopy(this.colors, 0, iArr, 0, iArr.length);
        this.gradient.clearColorFilter();
        this.gradient.setColors(iArr);
        this.gradient.setOrientation(this.orientation);
        this.gradient.setGradientType(this.gradientType);
        if (this.gradientType == 1) {
            this.gradient.setGradientRadius((float) this.radius.getProgress());
        }
        this.bitmap = Bitmap.createBitmap(this.point.x, this.point.y, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(this.bitmap);
        this.gradient.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        this.gradient.draw(canvas);
        this.imageView.setImageBitmap(this.bitmap);
    }

    /* access modifiers changed from: private */
    public int getActualLengthOfColorsArray() {
        int i = 0;
        int i2 = 0;
        while (true) {
            int[] iArr = this.colors;
            if (i2 >= iArr.length) {
                break;
            } else if (iArr[i2] == -1) {
                i = i2;
                break;
            } else {
                i2++;
            }
        }
        if (i == 0) {
            return 5;
        }
        return i;
    }

    private int getRandomNumber() {
        return new Random().nextInt(256);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        hideNavigationBar();
    }

    /* access modifiers changed from: private */
    public void hideNavigationBar() {
        View decorView = getWindow().getDecorView();
        if (Build.VERSION.SDK_INT >= 19) {
            decorView.setSystemUiVisibility(4866);
        }
    }

    /* access modifiers changed from: private */
    public void saveGradient() {
//        if (!this.sharedPreferences.getBoolean("ad_free", false)) {
//            Constants.showUnlockPremiumHelperDialog(this).setOnDismissListener(new DialogInterface.OnDismissListener() {
//                public void onDismiss(DialogInterface dialogInterface) {
//                    GradientMakerActivity.this.hideNavigationBar();
//                }
//            });
//            return;
//        }
        Toast.makeText(this, "Saving...", 1).show();
        AsyncTask.execute(new Runnable() {
            public void run() {
                try {
                        String format = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                        String fname="Gradiant_"+format+".png";
                        final File file = new File (myDir, fname);
                        if (file.exists()){
                            Toast.makeText(getApplicationContext(), "You Already Downloaded This ImageFavorite Please Check MyDownload", Toast.LENGTH_SHORT).show();
                        }else {
                            StringBuilder rajesh_uri=new StringBuilder();
                            rajesh_uri.append(Environment.getExternalStorageDirectory());
                            rajesh_uri.append("/");
                            rajesh_uri.append(Glob.Edit_Folder_name);
                            rajesh_uri.append("/");
                            rajesh_uri.append(fname);

                            String myImageUri=rajesh_uri.toString();
                            Glob._url=myImageUri;

                            if (file.exists ())
                                file.delete ();
                            try {
                                FileOutputStream out = new FileOutputStream(file);
                                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
                                out.flush();
                                out.close();
                                getApplicationContext().sendBroadcast(new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE", Uri.fromFile(file)));


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                    FileOutputStream fileOutputStream = new FileOutputStream(file);
                    GradientMakerActivity.this.bitmap.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);
                    fileOutputStream.close();
                    GradientMakerActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(GradientMakerActivity.this, "Saved!", 0).show();
                            Constants.scanFile(GradientMakerActivity.this, file);
                        }
                    });
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void showColorPickerDialog(final View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View inflate = LayoutInflater.from(this).inflate(R.layout.color_picker_dialog, null);
        builder.setView(inflate);
        builder.setPositiveButton((CharSequence) "OK", (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                int rgb = Color.rgb(GradientMakerActivity.this.r.getProgress(), GradientMakerActivity.this.g.getProgress(), GradientMakerActivity.this.b.getProgress());
                if (view.getId() == R.id.color_one) {
                    GradientMakerActivity.this.colors[0] = rgb;
                    GradientMakerActivity.this.colorOne.setColorFilter(rgb);
                    GradientMakerActivity.this.colorOneRGB.setRed(GradientMakerActivity.this.r.getProgress());
                    GradientMakerActivity.this.colorOneRGB.setGreen(GradientMakerActivity.this.g.getProgress());
                    GradientMakerActivity.this.colorOneRGB.setBlue(GradientMakerActivity.this.b.getProgress());
                    GradientMakerActivity.this.invalidateGradient();
                } else if (view.getId() == R.id.color_two) {
                    GradientMakerActivity.this.colors[1] = rgb;
                    GradientMakerActivity.this.colorTwo.setColorFilter(rgb);
                    GradientMakerActivity.this.colorTwoRGB.setRed(GradientMakerActivity.this.r.getProgress());
                    GradientMakerActivity.this.colorTwoRGB.setGreen(GradientMakerActivity.this.g.getProgress());
                    GradientMakerActivity.this.colorTwoRGB.setBlue(GradientMakerActivity.this.b.getProgress());
                    GradientMakerActivity.this.invalidateGradient();
                } else if (view.getId() == R.id.color_three) {
                    GradientMakerActivity.this.colors[2] = rgb;
                    GradientMakerActivity.this.colorThree.setColorFilter(rgb);
                    GradientMakerActivity.this.colorThreeRGB.setRed(GradientMakerActivity.this.r.getProgress());
                    GradientMakerActivity.this.colorThreeRGB.setGreen(GradientMakerActivity.this.g.getProgress());
                    GradientMakerActivity.this.colorThreeRGB.setBlue(GradientMakerActivity.this.b.getProgress());
                    GradientMakerActivity.this.invalidateGradient();
                } else if (view.getId() == R.id.color_four) {
                    GradientMakerActivity.this.colors[3] = rgb;
                    GradientMakerActivity.this.colorFour.setColorFilter(rgb);
                    GradientMakerActivity.this.colorFourRGB.setRed(GradientMakerActivity.this.r.getProgress());
                    GradientMakerActivity.this.colorFourRGB.setGreen(GradientMakerActivity.this.g.getProgress());
                    GradientMakerActivity.this.colorFourRGB.setBlue(GradientMakerActivity.this.b.getProgress());
                    GradientMakerActivity.this.invalidateGradient();
                } else if (view.getId() == R.id.color_five) {
                    GradientMakerActivity.this.colors[4] = rgb;
                    GradientMakerActivity.this.colorFive.setColorFilter(rgb);
                    GradientMakerActivity.this.colorFiveRGB.setRed(GradientMakerActivity.this.r.getProgress());
                    GradientMakerActivity.this.colorFiveRGB.setGreen(GradientMakerActivity.this.g.getProgress());
                    GradientMakerActivity.this.colorFiveRGB.setBlue(GradientMakerActivity.this.b.getProgress());
                    GradientMakerActivity.this.invalidateGradient();
                }
                dialogInterface.dismiss();
            }
        });
        builder.setNegativeButton((CharSequence) "Cancel", (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        if (!(view.getId() == R.id.color_one || view.getId() == R.id.color_two)) {
            builder.setNeutralButton((CharSequence) "CLEAR", (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    boolean z = false;
                    if (view.getId() == R.id.color_three && GradientMakerActivity.this.colors[3] == -1 && GradientMakerActivity.this.colors[4] == -1) {
                        GradientMakerActivity.this.colors[2] = -1;
                        GradientMakerActivity.this.colorThree.clearColorFilter();
                    } else if (view.getId() == R.id.color_four && GradientMakerActivity.this.colors[4] == -1) {
                        GradientMakerActivity.this.colors[3] = -1;
                        GradientMakerActivity.this.colorFour.clearColorFilter();
                    } else if (view.getId() == R.id.color_five) {
                        GradientMakerActivity.this.colors[4] = -1;
                        GradientMakerActivity.this.colorFive.clearColorFilter();
                    } else {
                        z = true;
                    }
                    if (z) {
                        Toast.makeText(GradientMakerActivity.this, "You need to clear the higher color first", 1).show();
                    } else {
                        GradientMakerActivity.this.invalidateGradient();
                    }
                    dialogInterface.dismiss();
                }
            });
        }
        this.r = (SeekBar) inflate.findViewById(R.id.r);
        this.g = (SeekBar) inflate.findViewById(R.id.g);
        this.b = (SeekBar) inflate.findViewById(R.id.b);
        this.rValue = (TextView) inflate.findViewById(R.id.r_value);
        this.gValue = (TextView) inflate.findViewById(R.id.g_value);
        this.bValue = (TextView) inflate.findViewById(R.id.b_value);
        this.r.setOnSeekBarChangeListener(this.onSeekBarChangeListener);
        this.g.setOnSeekBarChangeListener(this.onSeekBarChangeListener);
        this.b.setOnSeekBarChangeListener(this.onSeekBarChangeListener);
        this.background = (RelativeLayout) inflate.findViewById(R.id.background);
        if (view.getId() == R.id.color_one) {
            this.r.setProgress(this.colorOneRGB.getRed());
            this.g.setProgress(this.colorOneRGB.getGreen());
            this.b.setProgress(this.colorOneRGB.getBlue());
        } else if (view.getId() == R.id.color_two) {
            this.r.setProgress(this.colorTwoRGB.getRed());
            this.g.setProgress(this.colorTwoRGB.getGreen());
            this.b.setProgress(this.colorTwoRGB.getBlue());
        } else if (view.getId() == R.id.color_three) {
            this.r.setProgress(this.colorThreeRGB.getRed());
            this.g.setProgress(this.colorThreeRGB.getGreen());
            this.b.setProgress(this.colorThreeRGB.getBlue());
        } else if (view.getId() == R.id.color_four) {
            this.r.setProgress(this.colorFourRGB.getRed());
            this.g.setProgress(this.colorFourRGB.getGreen());
            this.b.setProgress(this.colorFourRGB.getBlue());
        } else if (view.getId() == R.id.color_five) {
            this.r.setProgress(this.colorFiveRGB.getRed());
            this.g.setProgress(this.colorFiveRGB.getGreen());
            this.b.setProgress(this.colorFiveRGB.getBlue());
        }
        this.background.setBackgroundColor(Color.rgb(this.r.getProgress(), this.g.getProgress(), this.b.getProgress()));
        AlertDialog create = builder.create();
        create.setCancelable(true);
        create.getWindow().setFlags(512, 512);
        create.setOnDismissListener(new DialogInterface.OnDismissListener() {
            public void onDismiss(DialogInterface dialogInterface) {
                GradientMakerActivity.this.hideNavigationBar();
            }
        });
        create.show();
    }

    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        if (i == R.id.type_linear) {
            this.gradientType = 0;
            if (this.orientations.getVisibility() != 0) {
                this.orientations.setVisibility(0);
            }
            if (this.radialOptions.getVisibility() != 4) {
                this.radialOptions.setVisibility(4);
            }
        } else if (i == R.id.type_radial) {
            this.gradientType = 1;
            if (this.orientations.getVisibility() != 4) {
                this.orientations.setVisibility(4);
            }
            if (this.radialOptions.getVisibility() != 0) {
                this.radialOptions.setVisibility(0);
            }
        } else {
            this.gradientType = 2;
            if (this.orientations.getVisibility() != 8) {
                this.orientations.setVisibility(8);
            }
            if (this.radialOptions.getVisibility() != 8) {
                this.radialOptions.setVisibility(8);
            }
        }
        invalidateGradient();
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        getWindow().setFormat(1);
    }
}

