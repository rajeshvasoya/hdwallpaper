package example.hdwallpaper.utils.other;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.media.MediaScannerConnection;
import android.media.MediaScannerConnection.OnScanCompletedListener;
import android.net.Uri;
import android.os.Build.VERSION;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AlertDialog.Builder;

import java.io.File;

public class Constants {
    public static final String BANNER_PLACEMENT_ID = "358900451375886_358900818042516";
    public static final String CATEGORY_DOMAIN = "http://www.367labs.a2hosted.com/category_img/";
    public static final String DOMAIN = "http://www.367labs.a2hosted.com/casual/";
    public static final String GET_TAGS = "http://www.367labs.a2hosted.com/script/casual/2.0/GET_TAGS.php";
    public static final String HOST_NAME = "http://www.367labs.a2hosted.com";
    public static final String INCREMENT_DOWNLOAD = "http://www.367labs.a2hosted.com/script/casual/2.0/incrementdownloads.php";
    public static final String INTERSTITIAL_PLACEMENT_ID = "358900451375886_362773140988617";
    public static final int OFFSET_LIMIT = 30;
    public static final String PRIVACY_POLICY = "http://www.367labs.a2hosted.com/policies/wallpie/privacy_policy.html";
    public static final String SHARED_PREFERENCES_NAME = "WallsPy";
    public static final String TEST_TOKEN = "";
    public static final String UPDATE_WALLPAPER = "http://www.367labs.a2hosted.com/script/casual/2.0/UPDATE_WALLPAPER.php";
    private Context context;

    public Constants(Context context2) {
        this.context = context2;
    }

    public String getFolderName() {
        return this.context.getSharedPreferences(SHARED_PREFERENCES_NAME, 0).getString("Folder", "/WallsPy/");
    }

    public static void scanFile(Context context2, File file) {
        if (VERSION.SDK_INT < 19) {
            context2.sendBroadcast(new Intent("android.intent.action.MEDIA_MOUNTED", Uri.parse(file.getAbsolutePath())));
            return;
        }
        MediaScannerConnection.scanFile(context2, new String[]{file.getAbsolutePath()}, null, new OnScanCompletedListener() {
            public void onScanCompleted(String str, Uri uri) {
            }
        });
    }

    public static boolean deleteDir(File file) {
        boolean z = false;
        if (file == null || !file.isDirectory()) {
            if (file != null && file.isFile() && file.delete()) {
                z = true;
            }
            return z;
        }
        for (String file2 : file.list()) {
            if (!deleteDir(new File(file, file2))) {
                return false;
            }
        }
        return file.delete();
    }

    public static Point getDeviceDimensions(Activity activity) {
        Point point = new Point();
        if (VERSION.SDK_INT >= 19) {
            activity.getWindowManager().getDefaultDisplay().getRealSize(point);
        } else {
            activity.getWindowManager().getDefaultDisplay().getSize(point);
        }
        return point;
    }

    public static AlertDialog showUnlockPremiumHelperDialog(final Context context2) {
        Builder builder = new Builder(context2);
        builder.setTitle((CharSequence) "Unlock Premium");
        builder.setMessage((CharSequence) "By unlocking premium, You will get\n\n ✔ Ad-Free experience.\n ✔ Premium features.\n ✔ Priority support.\n");
        builder.setPositiveButton((CharSequence) "UNLOCK", (OnClickListener) new OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.setNegativeButton((CharSequence) "LATER", (OnClickListener) new OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.setNeutralButton((CharSequence) "RESTORE PURCHASE", (OnClickListener) new OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog create = builder.create();
        create.show();
        return create;
    }

    public static Bitmap cropBitmapFromCenterAndScreenSize(Context context2, Bitmap bitmap) {
        int i;
        int i2;
        int i3;
        int i4;
        try {
            float width = (float) bitmap.getWidth();
            float height = (float) bitmap.getHeight();
            Point deviceDimensions = getDeviceDimensions((Activity) context2);
            if (deviceDimensions.y > deviceDimensions.x) {
                i2 = deviceDimensions.y;
                i = deviceDimensions.x;
            } else {
                i2 = deviceDimensions.x;
                i = deviceDimensions.y;
            }
            float f = width / height;
            if (((float) (i / i2)) > f) {
                i3 = (int) (((float) i) / f);
                i4 = i;
            } else {
                i4 = (int) (((float) i2) * f);
                i3 = i2;
            }
            return Bitmap.createBitmap(Bitmap.createScaledBitmap(bitmap, i4, i3, true), (int) (((float) (i4 - i)) / 2.0f), (int) (((float) (i3 - i2)) / 2.0f), i, i2);
        } catch (Exception unused) {
            return bitmap;
        }
    }
}
