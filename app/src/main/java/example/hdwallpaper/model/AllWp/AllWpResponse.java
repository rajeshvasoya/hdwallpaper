
package example.hdwallpaper.model.AllWp;

import com.google.gson.annotations.Expose;

import java.util.List;

public class AllWpResponse {

    @Expose
    private List<AllWpData> data;

    public List<AllWpData> getData() {
        return data;
    }

    public void setData(List<AllWpData> data) {
        this.data = data;
    }

}
