package example.hdwallpaper.model.RgbCol;

public class RGBColor {
    private int B;
    private int G;
    private int R;
    private int S;

    public RGBColor(int i, int i2, int i3, int i4) {
        this.R = i;
        this.G = i2;
        this.B = i3;
        this.S = i4;
    }

    public int getR() {
        return this.R;
    }

    public int getG() {
        return this.G;
    }

    public int getB() {
        return this.B;
    }

    public int getS() {
        return this.S;
    }
}
