
package example.hdwallpaper.model.LatestWp;

import com.google.gson.annotations.Expose;

import java.util.List;

public class LatestWpResponse {

    @Expose
    private List<LatestWpData> data;

    public List<LatestWpData> getData() {
        return data;
    }

    public void setData(List<LatestWpData> data) {
        this.data = data;
    }

}
