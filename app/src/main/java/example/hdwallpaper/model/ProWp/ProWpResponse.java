package example.hdwallpaper.model.ProWp;

import com.google.gson.annotations.Expose;

import java.util.List;

import example.hdwallpaper.model.LatestWp.LatestWpData;

public class ProWpResponse {
    @Expose
    private List<ProWpData> data;

    public List<ProWpData> getData() {
        return data;
    }

    public void setData(List<ProWpData> data) {
        this.data = data;
    }
}
