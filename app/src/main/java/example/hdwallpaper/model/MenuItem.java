package example.hdwallpaper.model;

public class MenuItem {
    String title;
    int imageId;
    private int iconId;


    public MenuItem(String title, int imageId, int iconId) {
        this.title = title;
        this.imageId = imageId;
        this.iconId = iconId;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }
    public int getIconId() {
        return iconId;
    }

    public void setIconId(int iconId) {
        this.iconId = iconId;
    }

}