package example.hdwallpaper.model.PopulerWp;

import com.google.gson.annotations.Expose;

import java.util.List;

import example.hdwallpaper.model.ProWp.ProWpData;

public class PopWpResponse {
    @Expose
    private List<PopWpData> data;

    public List<PopWpData> getData() {
        return data;
    }

    public void setData(List<PopWpData> data) {
        this.data = data;
    }
}
