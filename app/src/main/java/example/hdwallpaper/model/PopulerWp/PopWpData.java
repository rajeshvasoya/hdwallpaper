
package example.hdwallpaper.model.PopulerWp;

import com.google.gson.annotations.SerializedName;

public class PopWpData {

    @SerializedName("cat_id")
    private String catId;
    @SerializedName("cat_name")
    private String catName;
    @SerializedName("pop_wp")
    private String popWp;
    @SerializedName("wp_id")
    private String wpId;
    @SerializedName("wp_imgname")
    private String wpImgname;
    @SerializedName("wp_imgpath")
    private String wpImgpath;
    @SerializedName("wp_time")
    private String wpTime;

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public String getPopWp() {
        return popWp;
    }

    public void setPopWp(String popWp) {
        this.popWp = popWp;
    }

    public String getWpId() {
        return wpId;
    }

    public void setWpId(String wpId) {
        this.wpId = wpId;
    }

    public String getWpImgname() {
        return wpImgname;
    }

    public void setWpImgname(String wpImgname) {
        this.wpImgname = wpImgname;
    }

    public String getWpImgpath() {
        return wpImgpath;
    }

    public void setWpImgpath(String wpImgpath) {
        this.wpImgpath = wpImgpath;
    }

    public String getWpTime() {
        return wpTime;
    }

    public void setWpTime(String wpTime) {
        this.wpTime = wpTime;
    }

}
