package example.hdwallpaper.model.WpCategory.CatWp;

import com.google.gson.annotations.Expose;

import java.util.List;

public class CatWpResponse {

    @Expose
    private List<CatWpData> data;

    public List<CatWpData> getData() {
        return data;
    }

    public void setData(List<CatWpData> data) {
        this.data = data;
    }
}
