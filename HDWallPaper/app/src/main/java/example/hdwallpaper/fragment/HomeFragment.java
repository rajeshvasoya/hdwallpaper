package example.hdwallpaper.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import example.hdwallpaper.R;
import example.hdwallpaper.adapter.MyAllWpAdapter;
import example.hdwallpaper.gradient.GradientMakerActivity;
import example.hdwallpaper.model.AllWp.AllWpResponse;
import example.hdwallpaper.model.AllWp.AllWpData;
import example.hdwallpaper.retrofit.Const;
import example.hdwallpaper.retrofit.RetrofitClient;
import example.hdwallpaper.retrofit.RetrofitService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment implements View.OnClickListener {

    Context context;
    private RecyclerView mAllWallRecycler;
    ProgressDialog pd;
    RetrofitService service;
    MyAllWpAdapter myAllWpAdapter;
    private RelativeLayout mGradient;
    private int page = 1;
    private boolean loading = true;
    private GridLayoutManager gridLayoutManager;
    ArrayList<AllWpData> allWpDataArrayList = new ArrayList<>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        context = getContext();

        View view = inflater.inflate(R.layout.fragment_home, container, false);
        service = RetrofitClient.getService();
        //allWpDataArrayList.clear();
        pd = new ProgressDialog(context);
        mGradient = (RelativeLayout) view.findViewById(R.id.gradient);
        mGradient.setOnClickListener(this);

        mAllWallRecycler = (RecyclerView) view.findViewById(R.id.AllWallRecycler);
        gridLayoutManager = new GridLayoutManager(context, 3);
        gridLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mAllWallRecycler.setLayoutManager(gridLayoutManager);
        myAllWpAdapter = new MyAllWpAdapter(getActivity(), allWpDataArrayList, service);
        mAllWallRecycler.setAdapter(myAllWpAdapter);

        pd.setTitle("Please Wait");
        pd.setMessage("Fetch Category");
        pd.show();
        fetchAllWallpaper(page);

        mAllWallRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) //check for scroll down
                {
                    int visibleItemCount = gridLayoutManager.getChildCount();
                    int totalItemCount = gridLayoutManager.getItemCount();
                    int pastVisiblesItems = gridLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = true;
                            page = page + 1;
                            fetchAllWallpaper(page);
                           // Toast.makeText(context, "Last Item Wow !", Toast.LENGTH_SHORT).show();
                            Log.v("...", "Last Item Wow !");
                            //Do pagination.. i.e. fetch new data
                        }
                    }
                }
            }
        });
        return view;
    }


    private void fetchAllWallpaper(int i) {

        Call<AllWpResponse> call = service.fetchallwallpapaer(String.valueOf(Const.FETCH_FLAG), String.valueOf(i));
        call.enqueue(new Callback<AllWpResponse>() {
            @Override
            public void onResponse(Call<AllWpResponse> call, Response<AllWpResponse> response) {
                allWpDataArrayList.addAll(response.body().getData());
                myAllWpAdapter.notifyDataSetChanged();
                Toast.makeText(context, ""+allWpDataArrayList.size(), Toast.LENGTH_SHORT).show();
                pd.dismiss();
            }

            @Override
            public void onFailure(Call<AllWpResponse> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(context, "No Data Found", Toast.LENGTH_SHORT).show();
                Log.d("er", t.toString());
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.gradient:
                startActivity(new Intent(getContext(), GradientMakerActivity.class));
                break;
            default:
                break;
        }
    }
}
