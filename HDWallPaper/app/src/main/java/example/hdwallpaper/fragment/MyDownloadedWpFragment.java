package example.hdwallpaper.fragment;


import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

import example.hdwallpaper.Album.MyGalleryadapter;
import example.hdwallpaper.Glob;
import example.hdwallpaper.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyDownloadedWpFragment extends Fragment {

    public static ArrayList<String> IMAGEALLARY = new ArrayList<>();
    private RecyclerView mMyDownloadRecycler;
    public MyGalleryadapter myGalleryadapter;
    private View view;

    public MyDownloadedWpFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_my_downloaded_wp, container, false);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        initView(view);
    }

    private void initView(@NonNull final View itemView) {
        IMAGEALLARY.clear();

        mMyDownloadRecycler = (RecyclerView) itemView.findViewById(R.id.myDownloadRecycler);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 3);
        gridLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mMyDownloadRecycler.setLayoutManager(gridLayoutManager);
        StringBuilder sb = new StringBuilder();
        sb.append(Environment.getExternalStorageDirectory().getAbsolutePath());
        String str = "/";
        sb.append(str);
        sb.append(Glob.Edit_Folder_name);
        sb.append(str);
        listAllImages(new File(sb.toString()));

        this.myGalleryadapter = new MyGalleryadapter(getContext(), IMAGEALLARY);
        this.mMyDownloadRecycler.setAdapter(myGalleryadapter);
    }

    private void listAllImages(File file) {
        File[] listFiles = file.listFiles();
        if (listFiles != null) {
            for (int length = listFiles.length - 1; length >= 0; length--) {
                String file2 = listFiles[length].toString();
                File file3 = new File(file2);
                StringBuilder sb = new StringBuilder();
                String str = "";
                sb.append(str);
                sb.append(file3.length());
                String sb2 = sb.toString();
                StringBuilder sb3 = new StringBuilder();
                sb3.append(str);
                sb3.append(file3.length());
                Log.d(sb2, sb3.toString());
                if (file3.length() <= 1024) {
                    Log.e("Invalid ImageFavorite", "Delete ImageFavorite");
                } else if (file3.toString().contains(".jpg") || file3.toString().contains(".png") || file3.toString().contains(".jpeg")) {
                    IMAGEALLARY.add(file2);
                    //Toast.makeText(getContext(), ""+IMAGEALLARY.size(), Toast.LENGTH_SHORT).show();
                }
                System.out.println(file2);
            }
            return;
        }
        System.out.println("Empty Folder");
    }
}
