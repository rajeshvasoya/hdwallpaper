package example.hdwallpaper.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import example.hdwallpaper.R;
import example.hdwallpaper.adapter.MyPopWpAdapter;
import example.hdwallpaper.model.PopulerWp.PopWpData;
import example.hdwallpaper.model.PopulerWp.PopWpResponse;
import example.hdwallpaper.retrofit.Const;
import example.hdwallpaper.retrofit.RetrofitClient;
import example.hdwallpaper.retrofit.RetrofitService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class MostPopulerWpFragment extends Fragment {

    Context context;
    private RecyclerView mPopWallRecycler;
    ProgressDialog pd;
    RetrofitService service;
    MyPopWpAdapter myPopWpAdapter;
    ArrayList<PopWpData> popWpDataArrayList = new ArrayList<>();

    public MostPopulerWpFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_most_populer_wp, container, false);
        service = RetrofitClient.getService();
        context = getContext();

        pd = new ProgressDialog(context);
        mPopWallRecycler = (RecyclerView) view.findViewById(R.id.popWallRecycler);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 3);
        gridLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mPopWallRecycler.setLayoutManager(gridLayoutManager);
        myPopWpAdapter = new MyPopWpAdapter(context, popWpDataArrayList, service);
        mPopWallRecycler.setAdapter(myPopWpAdapter);

        pd.setTitle("Please Wait");
        pd.setMessage("Fetch Category");
        pd.show();
        fetchPopulerWallpaper();
        return view;
    }

    private void fetchPopulerWallpaper() {
        Call<PopWpResponse> call = service.fetchPopWallPapaer(String.valueOf(Const.FETCH_FLAG));
        call.enqueue(new Callback<PopWpResponse>() {
            @Override
            public void onResponse(Call<PopWpResponse> call, Response<PopWpResponse> response) {
                popWpDataArrayList.addAll(response.body().getData());
                myPopWpAdapter.notifyDataSetChanged();
                pd.dismiss();
            }

            @Override
            public void onFailure(Call<PopWpResponse> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(context, "No Data Found", Toast.LENGTH_SHORT).show();
                Log.d("er", t.toString());
            }
        });
    }

}
