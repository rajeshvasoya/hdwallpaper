package example.hdwallpaper.Album;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.WallpaperManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.balysv.materialripple.MaterialRippleLayout;
import com.bumptech.glide.Glide;
import com.tapadoo.alerter.Alerter;

import java.io.File;
import java.util.ArrayList;

import example.hdwallpaper.Glob;
import example.hdwallpaper.MainActivity;
import example.hdwallpaper.R;
import example.hdwallpaper.fragment.MyDownloadedWpFragment;

public class MyGalleryadapter extends RecyclerView.Adapter<MyGalleryadapter.MyViewHolder> {

    private MyDownloadedWpFragment myDownloadedWpFragment;
    private Context context;
    private ArrayList<String> imageallary;
    private Dialog mydialog;
    private Bitmap bitmap2;

    public MyGalleryadapter(Context context, ArrayList<String> imageallary) {
        this.context = context;
        this.imageallary = imageallary;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.wallpaper_raw_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        Glide.with(context).load((String) imageallary.get(position)).into(holder.my_imageview);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            private TouchImageView mImageIv;
            private ImageButton mBtnDel;
            private ImageButton mWallpaperSet;
            private ImageButton mBtnShare;
            String str = " Created By : ";
            String str2 = "example.hdwallpaper.fileprovider";
            String str3 = "android.intent.extra.STREAM";
            String str4 = "android.intent.extra.TEXT";
            String str5 = "image/*";
            String str6 = "android.intent.action.SEND";

            @SuppressLint("ResourceType")
            @Override
            public void onClick(View v) {
                mydialog = new Dialog(context, 16973839);
                DisplayMetrics displayMetrics = new DisplayMetrics();
                ((MainActivity) context).getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                        WindowManager.LayoutParams.FLAG_FULLSCREEN);
                ((MainActivity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                double d = (double) displayMetrics.heightPixels;
                Double.isNaN(d);
                int i2 = (int) (d * 1.0d);
                double d2 = (double) displayMetrics.widthPixels;
                Double.isNaN(d2);
                int i3 = (int) (d2 * 1.0d);
                mydialog.requestWindowFeature(1);
                mydialog.getWindow().setFlags(1024, 1024);
                mydialog.setContentView(R.layout.activity_full_screen_view);
                mydialog.getWindow().setLayout(i3, i2);
                mydialog.setCanceledOnTouchOutside(true);

                ImageView imageView= mydialog.findViewById(R.id.iv_image);
                imageView.setImageURI(Uri.parse(imageallary.get(position)));
                initView(mydialog);
                bitmap2 = ((BitmapDrawable)imageView.getDrawable()).getBitmap();
                mydialog.show();
            }

            private void initView(@NonNull final Dialog itemView) {

                mImageIv = (TouchImageView) itemView.findViewById(R.id.iv_image);
                mBtnShare = (ImageButton) itemView.findViewById(R.id.btnShare);

                /*Share*/
                mBtnShare.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent2 = new Intent(str6);
                        intent2.setType(str5);
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append(Glob.app_name);
                        sb2.append(" Create By : ");
                        sb2.append(Glob.app_link);
                        intent2.putExtra(str4, sb2.toString());
                        intent2.putExtra(str3, FileProvider.getUriForFile(context, str2, new File(imageallary.get(position))));
                        context.startActivity(Intent.createChooser(intent2, "Share Image using"));
                    }
                });

                mWallpaperSet = (ImageButton) itemView.findViewById(R.id.set_wallpaper);
                /*set Wap*/
                mWallpaperSet.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        applyWallpaper();
                    }
                });

                /*del dilog*/
                mBtnDel = (ImageButton) itemView.findViewById(R.id.btnDel);
                mBtnDel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                        alertDialog.setTitle("Alert");
                        alertDialog.setMessage("Are you sure you want to Delete ");
                        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        File file = new File((String) MyGalleryadapter.this.imageallary.get(position));
                                        if (file.exists()) {
                                            file.delete();
                                        }
                                        MyGalleryadapter.this.imageallary.remove(position);
                                        MyGalleryadapter.this.notifyDataSetChanged();

                                        if (MyGalleryadapter.this.imageallary.size() == 0) {
                                            Toast.makeText(context, "No Image Found..", Toast.LENGTH_LONG).show();
                                        }
                                        mydialog.dismiss();
                                    }
                                });
                        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {

                                        dialog.dismiss();
                                    }
                                });
                        alertDialog.show();
                    }
                });
            }


        });
    }


    /* access modifiers changed from: private */
    public void applyWallpaper() {
        String str = "%";
        String str2 = "Set this wallpaper on?";
        if (Build.VERSION.SDK_INT >= 24) {
            androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(context);
            builder.setTitle((CharSequence) str2);
            View inflate = ((MainActivity)context).getLayoutInflater().inflate(R.layout.onlysetwallpaper, null);
            MaterialRippleLayout materialRippleLayout = (MaterialRippleLayout) inflate.findViewById(R.id.Home);
            MaterialRippleLayout materialRippleLayout2 = (MaterialRippleLayout) inflate.findViewById(R.id.Lock);
            MaterialRippleLayout materialRippleLayout3 = (MaterialRippleLayout) inflate.findViewById(R.id.Both);
            builder.setView(inflate);
            final androidx.appcompat.app.AlertDialog create = builder.create();
            create.setCancelable(true);
            materialRippleLayout.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    create.dismiss();
                   downloadAndSet("homescreen");
                }
            });
            materialRippleLayout2.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    create.dismiss();
                    downloadAndSet("lockscreen");
                }
            });
            materialRippleLayout3.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    create.dismiss();
                    downloadAndSet("bothscreen");
                }
            });

            create.getWindow().setFlags(512, 512);
            create.setOnDismissListener(new DialogInterface.OnDismissListener() {
                public void onDismiss(DialogInterface dialogInterface) {
                    // DisplayActivity.this.hideNavigationBar();
                }
            });
            create.show();
            return;
        }
        androidx.appcompat.app.AlertDialog.Builder builder2 = new androidx.appcompat.app.AlertDialog.Builder(context);
        builder2.setTitle((CharSequence) str2);
        View inflate2 = ((MainActivity)context).getLayoutInflater().inflate(R.layout.setwallpaper_just_home, null);
        MaterialRippleLayout materialRippleLayout4 = (MaterialRippleLayout) inflate2.findViewById(R.id.Home);
        builder2.setView(inflate2);
        final androidx.appcompat.app.AlertDialog create2 = builder2.create();
        create2.setCancelable(true);
        materialRippleLayout4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                create2.dismiss();
                downloadAndSet("homescreen");
            }
        });
        create2.getWindow().setFlags(512, 512);
        create2.setOnDismissListener(new DialogInterface.OnDismissListener() {
            public void onDismiss(DialogInterface dialogInterface) {
                //DisplayActivity.this.hideNavigationBar();
            }
        });
        create2.show();
    }

    public void downloadAndSet(String str) {
        String str2 = "Please wait...";
        if (str.equalsIgnoreCase("homescreen")) {
            Alerter.create((MainActivity)context).setTitle(str2).setText("Setting wallpaper on your homescreen").setBackgroundColor(R.color.black).setDuration(1500).show();
            setAsWallpaper("home");
        } else if (str.equalsIgnoreCase("lockscreen")) {
            Alerter.create((MainActivity)context).setTitle(str2).setText("Setting wallpaper on your lockscreen").setBackgroundColor(R.color.black).setDuration(1500).show();
            setAsWallpaper("lock");
        } else {
            Alerter.create((MainActivity)context).setTitle(str2).setText("Setting wallpaper on both screens").setBackgroundColor(R.color.black).setDuration(1500).show();
            setAsWallpaper("both");
        }
    }

    public void setAsWallpaper(final String str) {

        final WallpaperManager instance = WallpaperManager.getInstance(context);

        new Thread(new Runnable() {
            @SuppressLint("WrongConstant")
            public void run() {
                try {
                    char c = 65535;
                    int hashCode = str.hashCode();
                    if (hashCode != 3208415) {
                        if (hashCode == 3327275) {
                            if (str.equals("lock")) {
                                c = 1;
                            }
                        }
                    } else if (str.equals("home")) {
                        c = 0;
                    }
                    if (c != 0) {
                        if (c != 1) {
                            if (Build.VERSION.SDK_INT >= 24) {
                                instance.setBitmap(bitmap2, null, true, 1);
                                instance.setBitmap(bitmap2, null, true, 2);
                            }
                        } else if (Build.VERSION.SDK_INT >= 24) {
                            instance.setBitmap(bitmap2, null, true, 2);
                        }
                    } else if (Build.VERSION.SDK_INT >= 24) {
                        instance.setBitmap(bitmap2, null, true, 1);
                    } else {
                        instance.setBitmap(bitmap2);
                    }
                    Alerter.create((MainActivity)context).setTitle("Wallpaper Updated!").setBackgroundColor(R.color.black).setDuration(1000).show();
                    mydialog.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    @Override
    public int getItemCount() {

        return imageallary.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView my_imageview;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            my_imageview = (ImageView) itemView.findViewById(R.id.my_imageview);
        }
    }
}