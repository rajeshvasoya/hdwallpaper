package example.hdwallpaper.activity;

import android.annotation.SuppressLint;
import android.app.WallpaperManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;
import androidx.viewpager.widget.ViewPager;

import com.balysv.materialripple.MaterialRippleLayout;
import com.bumptech.glide.Glide;
import com.ocnyang.pagetransformerhelp.cardtransformer.AlphaAndScalePageTransformer;
import com.tapadoo.alerter.Alerter;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import example.hdwallpaper.Glob;
import example.hdwallpaper.MainActivity;
import example.hdwallpaper.R;
import example.hdwallpaper.adapter.MyTryalPagerAdapter;
import example.hdwallpaper.model.MyFav.FavoriteDatabase;
import example.hdwallpaper.model.MyFav.FavoriteList;
import example.hdwallpaper.model.WPData.WpData;
import example.hdwallpaper.retrofit.Const;
import jp.wasabeef.glide.transformations.BlurTransformation;

import static com.bumptech.glide.request.RequestOptions.bitmapTransform;
import static example.hdwallpaper.Glob.myDir;

public class TryalDisplayActivity extends AppCompatActivity implements View.OnClickListener {

    ArrayList<WpData> wpData = new ArrayList<>();

    private ViewPager mViewPager;
    ImageView myBlureImg, myClearImg;
    Animation animFadeIn;
    ImageButton msetWallpaper, mcustom, mfavorite;
    public static int pagerPosition;
    private TextView downloads;
    private FrameLayout mImageSaveFrame;
    private ImageButton mWallpaperSet;
    private TextView mTextSet;
    public static Bitmap mainframebitmap;

    FavoriteList favoriteList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_display);

       // wpData.clear();
        wpData = (ArrayList<WpData>) getIntent().getSerializableExtra("wpData");
        Toast.makeText(this, "" + wpData.size(), Toast.LENGTH_SHORT).show();

        initView();


        //Glob.myWallpapers=new ArrayList<>();
        animFadeIn = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_in);
        myBlureImg = (ImageView) findViewById(R.id.myBlureImg);
        myClearImg = (ImageView) findViewById(R.id.myClearImg);


        downloads = (TextView) findViewById(R.id.downloads);
        downloads.setText(wpData.get(pagerPosition).getPopWp());
        mViewPager = ((ViewPager) findViewById(R.id.cardViewPager));
        mViewPager.setOffscreenPageLimit(3);
        mViewPager.setAdapter(new MyTryalPagerAdapter(wpData, this, mViewPager));
        mViewPager.setPageMargin(40);
        mViewPager.setPageTransformer(true, new AlphaAndScalePageTransformer());
        mViewPager.setCurrentItem(Glob.wpPosition);

        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                pagerPosition = mViewPager.getCurrentItem();
                myBlureImg.startAnimation(animFadeIn);
                downloads.setText(wpData.get(pagerPosition).getPopWp());
                Glide.with(getApplicationContext())
                        .load(Const.BASE_URL + wpData.get(pagerPosition).getWpImgpath())
                        .apply(bitmapTransform(new BlurTransformation(10, 30)))
                        .into(myBlureImg);
                Glide.with(getApplicationContext())
                        .load(Const.BASE_URL + wpData.get(pagerPosition).getWpImgpath())
                        .into(myClearImg);
                String wpid= wpData.get(pagerPosition).getWpId();
                if (Glob.favoriteDatabase.favoriteDao().isFavorite(Integer.parseInt(wpid))==1){
                    mfavorite.setImageResource(R.drawable.ic_favorite_white);
                }else {
                    mfavorite.setImageResource(R.drawable.ic_favorite);
                }

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {


            }
        });


    }

    private void initView() {

        mImageSaveFrame = (FrameLayout) findViewById(R.id.imageSaveFrame);

        msetWallpaper = (ImageButton) findViewById(R.id.set_wallpaper);
        msetWallpaper.setOnClickListener(this);
        mcustom = (ImageButton) findViewById(R.id.custom);
        mcustom.setOnClickListener(this);
        mfavorite = (ImageButton) findViewById(R.id.favorite);
        mfavorite.setOnClickListener(this);
        mTextSet = (TextView) findViewById(R.id.set_text);



    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.custom:
                mainframebitmap = getbitmap(mImageSaveFrame);
                startActivity(new Intent(this, EditWpActivity.class));
                break;
            case R.id.favorite:
                favoriteList=new FavoriteList();
                String wpid= wpData.get(pagerPosition).getWpId();
                favoriteList.setWpId(wpid);
                favoriteList.setCatId(wpData.get(pagerPosition).getCatId());
                favoriteList.setCatName(wpData.get(pagerPosition).getCatName());
                favoriteList.setWpImgname(wpData.get(pagerPosition).getWpImgname());
                favoriteList.setWpImgpath(wpData.get(pagerPosition).getWpImgpath());
                favoriteList.setPopWp(wpData.get(pagerPosition).getPopWp());
                favoriteList.setWpTime(wpData.get(pagerPosition).getWpTime());
                if ( Glob.favoriteDatabase.favoriteDao().isFavorite(Integer.parseInt(wpid))!=1){
                    mfavorite.setImageResource(R.drawable.ic_favorite_white);
                    Glob.favoriteDatabase.favoriteDao().addData(favoriteList);
                }else {
                    mfavorite.setImageResource(R.drawable.ic_favorite);
                    Glob.favoriteDatabase.favoriteDao().delete(favoriteList);

                }

                break;
            case R.id.set_wallpaper:
//                Toast.makeText(this, "save", Toast.LENGTH_SHORT).show();
                //
                applyWallpaper();
                break;
            default:
                break;
        }
    }


    /* access modifiers changed from: private */
    public void applyWallpaper() {
        String str = "%";
        String str2 = "Set this wallpaper on?";
        if (Build.VERSION.SDK_INT >= 24) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle((CharSequence) str2);
            View inflate = getLayoutInflater().inflate(R.layout.setwallpaper, null);
            MaterialRippleLayout materialRippleLayout = (MaterialRippleLayout) inflate.findViewById(R.id.Home);
            MaterialRippleLayout materialRippleLayout2 = (MaterialRippleLayout) inflate.findViewById(R.id.Lock);
            MaterialRippleLayout materialRippleLayout3 = (MaterialRippleLayout) inflate.findViewById(R.id.Both);
            MaterialRippleLayout materialRippleLayout4 = (MaterialRippleLayout) inflate.findViewById(R.id.imgDwnload);
            builder.setView(inflate);
            final AlertDialog create = builder.create();
            create.setCancelable(true);
            materialRippleLayout.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    create.dismiss();
                    TryalDisplayActivity.this.downloadAndSet("homescreen");
                }
            });
            materialRippleLayout2.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    create.dismiss();
                    TryalDisplayActivity.this.downloadAndSet("lockscreen");
                }
            });
            materialRippleLayout3.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    create.dismiss();
                    TryalDisplayActivity.this.downloadAndSet("bothscreen");
                }
            });
            materialRippleLayout4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    create.dismiss();
                    mainframebitmap = getbitmap(mImageSaveFrame);
                    saveImage(mainframebitmap);
                }
            });
            create.getWindow().setFlags(512, 512);
            create.setOnDismissListener(new DialogInterface.OnDismissListener() {
                public void onDismiss(DialogInterface dialogInterface) {
                    // TryalDisplayActivity.this.hideNavigationBar();
                }
            });
            create.show();
            return;
        }
        AlertDialog.Builder builder2 = new AlertDialog.Builder(this);
        builder2.setTitle((CharSequence) str2);
        View inflate2 = getLayoutInflater().inflate(R.layout.setwallpaper_just_home, null);
        MaterialRippleLayout materialRippleLayout4 = (MaterialRippleLayout) inflate2.findViewById(R.id.Home);
        builder2.setView(inflate2);
        final AlertDialog create2 = builder2.create();
        create2.setCancelable(true);
        materialRippleLayout4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                create2.dismiss();
                TryalDisplayActivity.this.downloadAndSet("homescreen");
            }
        });
        create2.getWindow().setFlags(512, 512);
        create2.setOnDismissListener(new DialogInterface.OnDismissListener() {
            public void onDismiss(DialogInterface dialogInterface) {
                //TryalDisplayActivity.this.hideNavigationBar();
            }
        });
        create2.show();
    }

    public void downloadAndSet(String str) {
        String str2 = "Please wait...";
        if (str.equalsIgnoreCase("homescreen")) {
            Alerter.create(this).setTitle(str2).setText("Setting wallpaper on your homescreen").setBackgroundColor(R.color.black).setDuration(1500).show();
            setAsWallpaper("home");
        } else if (str.equalsIgnoreCase("lockscreen")) {
            Alerter.create(this).setTitle(str2).setText("Setting wallpaper on your lockscreen").setBackgroundColor(R.color.black).setDuration(1500).show();
            setAsWallpaper("lock");
        } else {
            Alerter.create(this).setTitle(str2).setText("Setting wallpaper on both screens").setBackgroundColor(R.color.black).setDuration(1500).show();
            setAsWallpaper("both");
        }
    }

    public void setAsWallpaper(final String str) {
        final Bitmap bitmap2;
        final WallpaperManager instance = WallpaperManager.getInstance(this);

        bitmap2 = ((BitmapDrawable) myClearImg.getDrawable()).getBitmap();

        new Thread(new Runnable() {
            @SuppressLint("WrongConstant")
            public void run() {
                try {
                    char c = 65535;
                    int hashCode = str.hashCode();
                    if (hashCode != 3208415) {
                        if (hashCode == 3327275) {
                            if (str.equals("lock")) {
                                c = 1;
                            }
                        }
                    } else if (str.equals("home")) {
                        c = 0;
                    }
                    if (c != 0) {
                        if (c != 1) {
                            if (Build.VERSION.SDK_INT >= 24) {
                                instance.setBitmap(bitmap2, null, true, 1);
                                instance.setBitmap(bitmap2, null, true, 2);
                            }
                        } else if (Build.VERSION.SDK_INT >= 24) {
                            instance.setBitmap(bitmap2, null, true, 2);
                        }
                    } else if (Build.VERSION.SDK_INT >= 24) {
                        instance.setBitmap(bitmap2, null, true, 1);
                    } else {
                        instance.setBitmap(bitmap2);
                    }
                    Alerter.create(TryalDisplayActivity.this).setTitle("Wallpaper Updated!").setBackgroundColor(R.color.black).setDuration(1000).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }


    private void saveImage(Bitmap bitmap) {

        String format = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());

        String fname = wpData.get(pagerPosition).getWpImgname();

        File file = new File(myDir, fname);
        if (file.exists()) {
            Toast.makeText(this, "You Already Downloaded This ImageFavorite Please Check MyDownload", Toast.LENGTH_SHORT).show();
        } else {
            StringBuilder rajesh_uri = new StringBuilder();
            rajesh_uri.append(Environment.getExternalStorageDirectory());
            rajesh_uri.append("/");
            rajesh_uri.append(Glob.Edit_Folder_name);
            rajesh_uri.append("/");
            rajesh_uri.append(fname);

            String myImageUri = rajesh_uri.toString();
            Glob._url = myImageUri;

            if (file.exists())
                file.delete();
            try {
                FileOutputStream out = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
                out.flush();
                out.close();
                getApplicationContext().sendBroadcast(new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE", Uri.fromFile(file)));
                Toast.makeText(TryalDisplayActivity.this, "Saved!", Toast.LENGTH_SHORT).show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }


    private Bitmap getbitmap(View view) {
        Bitmap createBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        view.draw(new Canvas(createBitmap));
        return createBitmap;
    }



}



