
package example.hdwallpaper.model.WpCategory.PopCat;

import com.google.gson.annotations.Expose;

import java.util.List;

public class PopCatResponse {

    @Expose
    private List<PopCatData> data;

    public List<PopCatData> getData() {
        return data;
    }

    public void setData(List<PopCatData> data) {
        this.data = data;
    }


}
