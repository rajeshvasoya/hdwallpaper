
package example.hdwallpaper.model.WpCategory.AllCat;

import com.google.gson.annotations.Expose;

import java.util.List;

public class AllCatResponse {

    @Expose
    private List<AllCatData> data;

    public List<AllCatData> getData() {
        return data;
    }

    public void setData(List<AllCatData> data) {
        this.data = data;
    }


}
