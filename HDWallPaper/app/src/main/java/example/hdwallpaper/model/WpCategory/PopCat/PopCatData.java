package example.hdwallpaper.model.WpCategory.PopCat;

import com.google.gson.annotations.SerializedName;

public class PopCatData {

        @SerializedName("cat_id")
        private String catId;
        @SerializedName("cat_imgname")
        private String catImgname;
        @SerializedName("cat_imgpath")
        private String catImgpath;
        @SerializedName("cat_name")
        private String catName;
        @SerializedName("cat_time")
        private String catTime;

        public String getCatId() {
            return catId;
        }

        public void setCatId(String catId) {
            this.catId = catId;
        }

        public String getCatImgname() {
            return catImgname;
        }

        public void setCatImgname(String catImgname) {
            this.catImgname = catImgname;
        }

        public String getCatImgpath() {
            return catImgpath;
        }

        public void setCatImgpath(String catImgpath) {
            this.catImgpath = catImgpath;
        }

        public String getCatName() {
            return catName;
        }

        public void setCatName(String catName) {
            this.catName = catName;
        }

        public String getCatTime() {
            return catTime;
        }

        public void setCatTime(String catTime) {
            this.catTime = catTime;
        }

    }