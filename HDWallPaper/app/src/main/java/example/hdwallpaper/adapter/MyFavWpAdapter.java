package example.hdwallpaper.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import example.hdwallpaper.Glob;
import example.hdwallpaper.R;
import example.hdwallpaper.activity.TryalDisplayActivity;
import example.hdwallpaper.model.LatestWp.LatestWpData;
import example.hdwallpaper.model.MyFav.FavoriteList;
import example.hdwallpaper.model.WPData.WpData;
import example.hdwallpaper.retrofit.Const;
import example.hdwallpaper.retrofit.RetrofitService;

public class MyFavWpAdapter extends RecyclerView.Adapter<MyFavWpAdapter.MyViewHolder> {


    ArrayList<WpData> wpData=new ArrayList<>();
    private Context context;
    private ArrayList<FavoriteList> favoriteLists;


    public MyFavWpAdapter(Context context, ArrayList<FavoriteList> favoriteLists) {
        this.context = context;
        this.favoriteLists = favoriteLists;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.wallpaper_raw_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        final FavoriteList data=favoriteLists.get(position);
        Glide.with(context)
                .load(Const.BASE_URL + data.getWpImgpath())
                .into(holder.my_imageview);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wpData.clear();
                for (int i=0;i<favoriteLists.size();i++){
                    WpData wpDatas=new WpData();
                    wpDatas.setWpId(favoriteLists.get(i).getWpId());
                    wpDatas.setCatId(favoriteLists.get(i).getCatId());
                    wpDatas.setCatName(favoriteLists.get(i).getCatName());
                    wpDatas.setWpImgname(favoriteLists.get(i).getWpImgname());
                    wpDatas.setWpImgpath(favoriteLists.get(i).getWpImgpath());
                    wpDatas.setWpTime(favoriteLists.get(i).getWpTime());
                    wpDatas.setPopWp(favoriteLists.get(i).getPopWp());
                    wpData.add(wpDatas);
                }
                Glob.wpPosition=position;
                Intent intent=new Intent(context,TryalDisplayActivity.class);
                intent.putExtra("wpData",wpData);
                context.startActivity(intent);
                //Toast.makeText(context, ""+wpData.size(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return favoriteLists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView my_imageview;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            my_imageview = (ImageView) itemView.findViewById(R.id.my_imageview);
        }
    }


}
