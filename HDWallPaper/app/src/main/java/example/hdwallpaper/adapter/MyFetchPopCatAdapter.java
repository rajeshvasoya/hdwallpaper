package example.hdwallpaper.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import example.hdwallpaper.Glob;
import example.hdwallpaper.R;
import example.hdwallpaper.activity.DisplayCatActivity;
import example.hdwallpaper.model.WpCategory.AllCat.AllCatResponse;
import example.hdwallpaper.model.WpCategory.PopCat.PopCatData;
import example.hdwallpaper.retrofit.Const;

public class MyFetchPopCatAdapter extends RecyclerView.Adapter<MyFetchPopCatAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<PopCatData> popCatDataArrayList;


    public MyFetchPopCatAdapter(Context context, ArrayList<PopCatData> popCatDataArrayList) {
        this.context = context;
        this.popCatDataArrayList = popCatDataArrayList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_image, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        Animation zoom_in2 = AnimationUtils.loadAnimation(context, R.anim.zoom_in2);
        final PopCatData data=popCatDataArrayList.get(position);
        Glide.with(context)
                .load(Const.BASE_URL+data.getCatImgpath())
                .into(holder.my_imageview);
        holder.my_imageview.startAnimation(zoom_in2);
        holder.txtCatName.setText(data.getCatName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Glob.parCatName=data.getCatName();
                context.startActivity(new Intent(context, DisplayCatActivity.class));
            }
        });
    }

    @Override
    public int getItemCount() {
        return popCatDataArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView my_imageview;
        TextView txtCatName;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            my_imageview =(ImageView) itemView.findViewById(R.id.image);
            txtCatName =(TextView) itemView.findViewById(R.id.txtCatName);
        }
    }
}
