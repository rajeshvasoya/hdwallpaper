package example.hdwallpaper;

import android.Manifest;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.room.Room;
import androidx.viewpager.widget.ViewPager;

import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import example.hdwallpaper.fragment.CategoryWpFragment;
import example.hdwallpaper.fragment.FavouritesWpFragment;
import example.hdwallpaper.fragment.HomeFragment;
import example.hdwallpaper.fragment.LatestWpFragment;
import example.hdwallpaper.fragment.MostPopulerWpFragment;
import example.hdwallpaper.fragment.MyDownloadedWpFragment;
import example.hdwallpaper.fragment.ProWpFragment;
import example.hdwallpaper.model.MenuItem;
import example.hdwallpaper.model.MyFav.FavoriteDatabase;
import example.hdwallpaper.widget.SNavigationDrawer;

public class MainActivity extends AppCompatActivity {
    SNavigationDrawer sNavigationDrawer;
    private FragmentPagerItemAdapter adapter;
    public static Fragment fragment;
    Class fragmentClass;
    int color1 = 0;
    Toolbar mTopToolbar;
    private ViewPager viewPager;
    private SmartTabLayout viewPagerTab;
   // private File myDir;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        checkPermission();
        createDirectories();
        Glob.favoriteDatabase= Room.databaseBuilder(getApplicationContext(),FavoriteDatabase.class,"myfavdb").allowMainThreadQueries().build();
        mTopToolbar = (Toolbar) findViewById(R.id.myToolbar);
        setSupportActionBar(mTopToolbar);



        final RelativeLayout relLayout1 = (RelativeLayout) findViewById(R.id.relLayout1);
        final RelativeLayout relLayout2 = (RelativeLayout) findViewById(R.id.relLayout2);
        final FrameLayout frameLayout = (FrameLayout) findViewById(R.id.frameLayout);
        setupViewPager();


        sNavigationDrawer = findViewById(R.id.navigationDrawer);
        List<MenuItem> menuItems = new ArrayList<>();
        menuItems.add(new MenuItem("Home", R.drawable.news_bg, R.drawable.ic_news));
        menuItems.add(new MenuItem("Pro", R.drawable.feed_bg, R.drawable.ic_news));
        menuItems.add(new MenuItem("Populer", R.drawable.message_bg, R.drawable.ic_news));
        menuItems.add(new MenuItem("Latest", R.drawable.music_bg, R.drawable.ic_news));
        menuItems.add(new MenuItem("Favourites", R.drawable.music_bg, R.drawable.ic_news));
        sNavigationDrawer.setMenuItemList(menuItems);
        fragmentClass = HomeFragment.class;
        try {
            fragment = (Fragment) fragmentClass.newInstance();
            mTopToolbar.setTitle("HD WallPaper");
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out).replace(R.id.frameLayout, fragment).commit();
        }

        sNavigationDrawer.setOnMenuItemClickListener(new SNavigationDrawer.OnMenuItemClickListener() {
            @Override
            public void onMenuItemClicked(int position) {
                System.out.println("Position " + position);

                switch (position) {
                    case 0: {
                        mTopToolbar.setTitle("Home");

                        relLayout1.setVisibility(View.VISIBLE);
                        relLayout2.setVisibility(View.VISIBLE);
                        frameLayout.setVisibility(View.GONE);

                        break;
                    }
                    case 1: {
                        mTopToolbar.setTitle("Pro Wallpaper");
                        relLayout1.setVisibility(View.GONE);
                        relLayout2.setVisibility(View.GONE);
                        frameLayout.setVisibility(View.VISIBLE);
                        fragmentClass = ProWpFragment.class;
                        break;
                    }
                    case 2: {
                        mTopToolbar.setTitle("Most Populer");
                        relLayout1.setVisibility(View.GONE);
                        relLayout2.setVisibility(View.GONE);
                        frameLayout.setVisibility(View.VISIBLE);
                        fragmentClass = MostPopulerWpFragment.class;
                        break;
                    }
                    case 3: {
                        mTopToolbar.setTitle("Latest Wallpaper");
                        relLayout1.setVisibility(View.GONE);
                        relLayout2.setVisibility(View.GONE);
                        frameLayout.setVisibility(View.VISIBLE);
                        fragmentClass = LatestWpFragment.class;
                        break;
                    }
                    case 4: {
                        mTopToolbar.setTitle("Favourite Wallpaper");
                        relLayout1.setVisibility(View.GONE);
                        relLayout2.setVisibility(View.GONE);
                        frameLayout.setVisibility(View.VISIBLE);
                        fragmentClass = FavouritesWpFragment.class;
                        break;
                    }


                }
                sNavigationDrawer.setDrawerListener(new SNavigationDrawer.DrawerListener() {

                    @Override
                    public void onDrawerOpened() {

                    }

                    @Override
                    public void onDrawerOpening() {

                    }

                    @Override
                    public void onDrawerClosing() {
                        System.out.println("Drawer closed");

                        try {
                            fragment = (Fragment) fragmentClass.newInstance();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        if (fragment != null) {
                            FragmentManager fragmentManager = getSupportFragmentManager();
                            fragmentManager.beginTransaction().setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out).replace(R.id.frameLayout, fragment).commit();

                        }
                    }

                    @Override
                    public void onDrawerClosed() {

                    }

                    @Override
                    public void onDrawerStateChanged(int newState) {
                        System.out.println("State " + newState);
                    }
                });
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menue_item, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull android.view.MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_UserProfile) {
            Toast.makeText(MainActivity.this, "Action clicked", Toast.LENGTH_LONG).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setupViewPager() {
        adapter = new FragmentPagerItemAdapter(
                getSupportFragmentManager(), FragmentPagerItems.with(this)
                .add("All", HomeFragment.class)
                .add("Catageory", CategoryWpFragment.class)
                .add("Pro", ProWpFragment.class)
                .add("MyDownload", MyDownloadedWpFragment.class)
                .create());


        viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(5);
        viewPagerTab = (SmartTabLayout) findViewById(R.id.tabs);
        viewPagerTab.setViewPager(viewPager);


    }

    private void checkPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CAMERA}, 1);
    }

    public void onRequestPermissionsResult(int i, @NonNull String[] strArr, @NonNull int[] iArr) {
        if (i != 1) {
            super.onRequestPermissionsResult(i, strArr, iArr);
            return;
        }
        if (iArr.length <= 0 || iArr[0] != 0) {
            Toast.makeText(this, "Permission denied to read your External storage", Toast.LENGTH_SHORT).show();
        }
    }

    private void createDirectories() {
        String root = Environment.getExternalStorageDirectory().toString();
        Glob.myDir = new File(root + "/"+ Glob.Edit_Folder_name);
        if (! Glob.myDir.exists()) {
            Glob.myDir.mkdirs();
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}
